import { Context } from './context'
import { MovementBlackBox } from './helpers/movement'
import {
    Zoom,
    Navigator,
    Viewport,
    Nodes,
    Connections,
    ConnectionMenu
} from './components'
import {
    initializeContext,
    updateConfiguration
} from './helpers/data'
import {
    Localization
} from './localization'

import './styles/index.css'
import { Configuration } from './configuration'
import { IConfiguration } from './constants/interfaces'
import './assets/icons/16-plus.svg'
import './assets/icons/16-change-color.svg'

/**
 * Mindmap Control v1
 */
export default class MindMap {

    /**
     * Global Context of control
     */
    private ctx: Context = Context.getInstance()

    /**
     * Global Context of control
     */
    public constructor(
        container: HTMLElement,
        initialConfig?: Partial<IConfiguration>
    ) {
        // Prepare root container
        container.classList.add('mindmap')

        // Initialize context in subclasses
        initializeContext()

        // Create loader
        const loader = document.createElement('loader')
        loader.className = 'mindmap-loader'
        
        container.appendChild(loader)
                
        // Store important things in context
        this.ctx.container = container
        
        if (initialConfig)
            updateConfiguration(initialConfig, true)
    
        // Init movement system
        this.ctx.movementBlackBox = new MovementBlackBox()

        this.ctx.localization = new Localization()
        
        this.ctx.localization.set(Configuration.locale).then(() => {
            this.initialize()    
        })
    }

    private initialize = () => {

        // Create control components
        new Zoom()
        new Viewport()
        new Navigator()
        new Connections()
        new Nodes()
        new ConnectionMenu()

        // Calculate initial zoom
        this.ctx.zoom.calculate()
        
        // Attach resize event
        this.ctx.attachWindowResize()
        
        // Refresh control adter initialization
        this.ctx.refresh()

        // Hide loader
        this.ctx.container.children[0].remove()
    }

    /**
     * Set data or configuration
     * @param config Set of props to set
     */
    public set = (config: Partial<IConfiguration>) => updateConfiguration(config)

    /**
     * Cause refresh of control
     */
    public refresh = () => this.ctx.refresh()
     
    /**
     * Cause full reload of control
     */
    public reload = () => this.ctx.reload()
     
    /**
     * Dispose control and remove all components
     */
    public dispose = () => this.ctx.dispose()

    public destroyColorPicker = () => {
        if (this.ctx.colorPicker)
            this.ctx.colorPicker.destroy()
        
        //if (this.ctx.menu.current) {
        //    this.ctx.menu.current.element.classList.remove('selected')
        //    this.ctx.menu.current = null
        //}
    }
}

export * from './constants'
export * from './constants/interfaces'