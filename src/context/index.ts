import { isNull } from '../helpers/main';
import { Configuration } from '../configuration';
import { EVENT, reiseEventAsync, reiseEvent } from '../events';
import { updateIndexes } from '../helpers/data';

/**
 * MindMap control global context
 */
export class Context {

    /**
     * Instance  of context ( global )
     */
    private static instance?: Context;

    /**
     * Creates an instance of context ( not used )
     */
    constructor() {}

    /**
     * Get current context instance
     * @returns instance 
     */
    public static getInstance = (): Context => {
        if (!Context.instance)
            Context.instance = new Context();

        return Context.instance;
    }

    /**
     * Removes instance and close singlenton
     */
    public static removeInstance = (): void => {
        if (this.instance)
            this.instance = undefined
    }

    /**
     * If control is already initialized
     */
    initialized: boolean

    /**
     * If control has any content
     */
    hasContent: boolean

    /**
     * Root view of control
     */
    viewport: any

    /**
     * View navigator
     */
    navigator: any

    /**
     * Zoom control
     */
    zoom: any

    /**
     * Nodes control
     */
    nodes: any

    /**
     * Connections control
     */
    connections: any

    /**
     * Positions of nodes in midnmap
     */
    positions: any

    /**
     * Context Menu
     */
    menu: any

    /**
     * Current data
     */
    data: any = {
        nodes: {},
        originalItems: [],
        connections: [],
        originalConnections: []
    }

    mapping: any = {}

    // Current rendered data
    renderedElements: any = {
        root: null,
        nodes: {},
        connections: {}
    }

    hasUpdate: boolean = false
    currentUpdate: any = {}

    /**
     * Items rendered/available in current view
     */
    itemsCount: number = 0
    
    /**
     * Root control container
     */
    container: any

    /**
     * Blackbox for user interactions
     */
    movementBlackBox: any

    colorPicker: any

    localization: any

    /**
     * Refresh control
     */
    refresh = () => {
        if (!this.viewport) return
        
        this.viewport.container.style.transform = `scale(${this.zoom.zoom.current})`

        this.viewport.scrollbars.horizontal.update();
        this.viewport.scrollbars.vertical.update();
        
        this.navigator.updateSelector();

        this.viewport.change({ x: 0, y: 0 }, true, true, null, false)

        this.nodes.render()
    }

    /**
     * Reload whole control
     */
    reload = (updateLocale?: boolean) => {
        
        reiseEvent(EVENT.onClean, {})

        if (!this.viewport) return
        
        this.viewport.container.style.transform = `scale(${this.zoom.zoom.current})`

        this.viewport.getSize()

        this.viewport.scrollbars.horizontal.update();
        this.viewport.scrollbars.vertical.update();
        
        this.navigator.updateSelector();

        this.viewport.change({ x: 0, y: 0 }, true, true, null, false)

        if (updateLocale) {
            this.menu.recreate()
        }

        // Update control data
        updateIndexes()

        // Clear nodes/connections layer
        this.nodes.clear()

        // Rerender data
        this.nodes.render()
    }

    /**
     * Set resize event
    */
    attachWindowResize = () => {
        
        // Handle window resizing
        window.onresize = () => this.reload()
    }

    /**
     * Dispose control and remove all elements
     */
    dispose = () => {

        // Remove resize event
        window.onresize = null
        
        // Deatach events
        this.movementBlackBox.dispose()

        // Remove root container
        this.container.remove()
        
        // Close context
        Context.removeInstance()
    }
}