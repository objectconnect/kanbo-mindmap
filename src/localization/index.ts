const importLocale = (lang) => import(`./${lang}.json`);

export let strings:any = {}

export class Localization {

    // Currently used language
    public locale:string = 'en'

    // Available strings
    public strings:any

    constructor() {    }

    async set(locale: string) {
        return new Promise((resolve, reject) => {
            if (!locale) {
                importLocale(locale).then((result) => {
                    this.strings = result
                    strings = this.strings
                    this.locale = locale
                    resolve(null)
                })
                return
            }
            
            this.strings = importLocale(locale).then((result) => {
                this.strings = result
                strings = this.strings
                this.locale = locale
                resolve(null)
            })
        })
    }
}
