import { Configuration } from "../../configuration"
import { generateFromTemplate, isNull } from '../../helpers/main'
import { Context } from '../../context'
import { Templates } from '../../templates'
import { IScrollbars, IViewportSize, XY } from "../../constants/interfaces"
import { Connection } from './connection'

/**
 * Mindmap viewport
 */
export class Connections {

    /**
     * Global Context of control
     */
    private ctx: Context = Context.getInstance()

    /**
     * Columns HTML container
     */
    container: any
    
    /**
     * Current viewport size and position
     */
    size: IViewportSize;
    
    /**
     * Creates an instance of zoom.
     */
    public constructor() {

        this.ctx.connections = this

        this.create()
        this.render()
    }

    create = () => {

        // Create connections container
        this.container = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        this.container.classList.value = 'mindmap-connections';

        this.ctx.viewport.container.prepend(this.container)
    }

    /**
     * Load current view size and position
     */
    render = () => { }

    update = (id: number, point?: XY, change?: XY) => {
        /*if (isNull(id) && !id) return

        const fromId = this.ctx.mapping.from[id]
        const toId = this.ctx.mapping.to[id]

        if(!isNull(fromId))
            fromId.map(fId => {
                this.ctx.renderedElements.connections[fId].start = point
                this.ctx.renderedElements.connections[fId].draw()    
            })

        if(!isNull(toId))
            toId.map(tId => {
                this.ctx.renderedElements.connections[tId].end = point
                this.ctx.renderedElements.connections[tId].draw()
            })*/
    }
}


// TODO: nodes without connection -> root node connection
// TODO: Rerender whole connection on every node movement