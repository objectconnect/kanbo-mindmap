import { Configuration } from "../../configuration"
import { generateFromTemplate, isNull } from '../../helpers/main'
import { Context } from '../../context'
import { Templates } from '../../templates'
import { XY } from "../../constants/interfaces"
import { reiseEvent, EVENT } from "../../events"
import { COLOR_PICKER_TARGET } from "../../constants"


/**
 * Color picker control
 */
export class ColorPicker {

    /**
     * Global Context of control
     */
    private ctx: Context = Context.getInstance()

    /**
     * Color picker HTML container
     */
    container: any
    list: any

    position: XY = {} as XY

    target: COLOR_PICKER_TARGET
    id: string
    callback: any
        
    /**
     * Creates an instance of color picker.
     */
    public constructor(
        position: XY,
        target: COLOR_PICKER_TARGET,
        id: string,
        callback?,
        container?) {
        
        this.id = id
        this.position = position
        this.target = target
        this.callback = callback

        this.create(container)
        this.render()
    }

    // Create color picker container
    create = (container?) => {
              
        this.container = generateFromTemplate(Templates.colorPicker.format(this.position.x, this.position.y))
        this.list = this.container.children[0]

        if (container)
            container.appendChild(this.container)
        else {
            this.ctx.container.appendChild(this.container)        
            this.container.style.position = 'absolute'
        }
    }

    /**
     * Load current view size and position
     */
    render = () => { 
        Configuration.colorPickerColors.map(c => {
            const color = document.createElement('span')
            color.setAttribute('data-color', c)
            color.style.backgroundColor = c
            color.onclick = this.onSelection

            this.list.appendChild(color)
        })

        reiseEvent(EVENT.onColorPickerRender, { id: this.id, target: this.target, container: this.container })
    }

    onSelection = (args) => {
        reiseEvent(EVENT.onColorChanged, { id: this.id, target: this.target, color: args.target.attributes['data-color'].value })

        this.container.remove()
        this.callback && this.callback()
    }

    destroy = () => {
        reiseEvent(EVENT.onColorPickerDestroy, { id: this.id, target: this.target })
        
        this.container.remove()
        this.callback && this.callback()
    }
}
