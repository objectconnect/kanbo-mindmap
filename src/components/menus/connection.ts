import { Configuration } from "../../configuration"
import { generateFromTemplate } from '../../helpers/main'
import { Context } from '../../context'
import { Templates } from '../../templates'
import { reiseEvent, EVENT } from "../../events"
import { ColorPicker } from "../colorpicker"
import { COLOR_PICKER_TARGET } from "../../constants"
import { strings } from '../../localization'

/**
 * Menu for connection
 */
export class ConnectionMenu {

    /**
     * Global Context of control
     */
    private ctx: Context = Context.getInstance()

     /**
      * Current element
      */
    element: HTMLElement

    /**
     * Current selected connection
     */
    current: any

    /**
     * Visibility state
    */
    isVisible: boolean

    deleteAction: any
    changeColorAction: any

    /**
     * Creates an instance of zoom.
     */
    public constructor() {

        this.ctx.menu = this // TODO

        this.create()
    }

    create = () => {

        // Genrate DOM structure
        this.element = generateFromTemplate(Templates.connectionMenu.format(strings.delete_action))

        this.deleteAction = this.element.children[1]
        this.changeColorAction = this.element.children[0]

        this.deleteAction.onclick = this.deleteConnection
        //this.changeColorAction.onclick = this.changeConnectionColor
        

        this.ctx.container.appendChild(this.element)
    }

    recreate = () => {
        const newElement = generateFromTemplate(Templates.connectionMenu.format(strings.delete_action))

        this.element.replaceWith(newElement)

        this.element = newElement

        this.deleteAction = this.element.children[1]
        this.changeColorAction = this.element.children[0]

        this.deleteAction.onclick = this.deleteConnection
    }

    changeConnectionColor = (args) => {        

        if (this.ctx.colorPicker) {
            this.ctx.colorPicker.destroy()
            this.ctx.colorPicker = null
        }

        const callback = () => {
            this.ctx.colorPicker = null
            this.toggle()    
        }

        this.ctx.colorPicker = new ColorPicker(
            { x: args.clientX - this.ctx.viewport.size.parentLeft, y: args.clientY - this.ctx.viewport.size.parentTop },
            COLOR_PICKER_TARGET.CONNECTION,
            this.current.data.id,
            callback,
            this.changeColorAction)
    }

    /**
     * Load current view size and position
     */
    deleteConnection = () => {
        if (!this.current) return
        
        // Change node data
        /*if (this.current.data.nodeId) {
            var node = this.ctx.renderedElements.nodes[this.current.data.nodeId].node;
            node.newRelationTarget = '-1';
            node.onMovementFinished();
        }*/
        if (this.current.data.end === -1) {
            this.ctx.renderedElements.nodes[this.current.data.start].node.delete()
        } else
            reiseEvent(EVENT.onConnectionDeleted, { start: this.current.data.start , end: this.current.data.end })

        this.toggle()
    }

    toggle = (x?, y?) => {
        this.isVisible = !this.isVisible;

        if (this.isVisible) {            
            this.changeConnectionColor({ clientX: 0, clientY:0 })
            this.element.style.top = `${y - this.ctx.viewport.size.parentTop}px`;
            this.element.style.left = `${x - this.ctx.viewport.size.parentLeft}px`;
            this.element.classList.add('visible');
        } else {
            this.element.classList.remove('visible');
            this.current.element.classList.remove('selected');
            this.current = null;

            if (this.ctx.colorPicker) {
                this.ctx.colorPicker.destroy()
                this.ctx.colorPicker = null
            }
        }
    }
}
