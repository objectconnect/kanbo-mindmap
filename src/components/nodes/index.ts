import { Configuration } from "../../configuration"
import { generateFromTemplate, isCollide, isNull } from '../../helpers/main'
import { Context } from '../../context'
import { Templates } from '../../templates'
import { IItemPosition, IScrollbars, IViewportSize } from "../../constants/interfaces"
import { Node } from './node'
import { textSpanIntersectsWithTextSpan } from "typescript"

/**
 * Mindmap viewport
 */
export class Nodes {

    /**
     * Global Context of control
     */
    private ctx: Context = Context.getInstance()

    /**
     * Columns HTML container
     */
    container: HTMLElement

     /**
      * Current element
      */
    element: HTMLElement

    rootNode: any

    /**
     * Creates an instance of zoom.
     */
    public constructor() {

        this.ctx.nodes = this

        this.render()
    }

    /**
     * Load current view size and position
     */
    render = () => {
        const { nodes } = this.ctx.renderedElements
        let { mapping } = this.ctx

        // Create root node
        this.renderRootNode()

        let newMapping = {}

        Object.keys(this.ctx.data.nodes).forEach(key => {
            const val = this.ctx.data.nodes[key]

            if (!nodes[val.id]) nodes[val.id] = { node: new Node(val) }
            else nodes[val.id].node.update(null, val)

            // Set new mapping
            newMapping[val.id] = true

            // Remove old mapping
            if (mapping[val.id]) delete mapping[val.id]

            if (!isNull(val.parentId))
                if (nodes[val.parentId] && nodes[val.parentId].node.data.children.indexOf(val.id) === -1)
                    nodes[val.parentId].node.data.children.push(val.id)
        })

        Object.keys(mapping).map(m => {
            this.ctx.renderedElements.nodes[m].node.connection.start = { x: 0, y: 0 }
            this.ctx.renderedElements.nodes[m].node.connection.end = { x: 0, y: 0 }
            this.ctx.renderedElements.nodes[m].node.connection.draw()
            this.ctx.renderedElements.nodes[m].node.remove()
            delete this.ctx.renderedElements.nodes[m]
        })

        this.ctx.mapping = newMapping

        this.ctx.navigator.update();
    }

    clear = () => {

        // Clear all rendered nodes
        this.ctx.renderedElements.nodes = {}

        // Store connections container
        const connectionContainer = this.ctx.viewport.container.children[0]

        // Clear connections container
        connectionContainer.innerHTML = ''

        // Clear viewport
        this.ctx.viewport.container.innerHTML = '' 

        // Restore connections container in DOM structure
        this.ctx.viewport.container.appendChild(connectionContainer)
    }

    renderRootNode =  () => {
        // TODO: if change then rerender root node
        if (this.rootNode) {
            if (JSON.stringify(this.rootNode) === JSON.stringify(Configuration.rootNode))
                return
            else {
                this.ctx.renderedElements.root.element.remove()
                this.rootNode = Configuration.rootNode
            }
        }

        const position = {
            x: (this.ctx.viewport.size.containerWidth / 2) - (Configuration.nodeSize.width / 2),
            y: (this.ctx.viewport.size.containerHeight / 2) - (Configuration.nodeSize.height / 2)
        };

        this.ctx.renderedElements.root = new Node({
            id: -1,
            css: [],
            position,
            data: { id: -1, title: Configuration.rootNode.name, ...position, color: Configuration.rootNode.color },
            canMove: false,
            isRoot: true
        })
    }

    findNewPosition = (position) => {
        
        // 1. Get side of root node true = left, false = right
        let sideOfRootNode: boolean = false

        // Check on which side new node will be placed at first
        if (position) {
            if (position.x > this.ctx.renderedElements.root.data.position.x)
                sideOfRootNode = true
        }

        // TODO: dynamic node height
        const nodeHeight = 70

        // Space to check
        const checkRect = {
            x: position.x - (Configuration.nodeSize.width + 50),
            y: position.y - (nodeHeight + 50),

            width: (3 * Configuration.nodeSize.width) + 100,
            height: (3 * nodeHeight) + 100
        }

        // Nodes neer current node
        const nodes = Object.values(this.ctx.positions).filter((p: IItemPosition) =>
            isCollide(checkRect, { ...p, width: Configuration.nodeSize.width, height: 50 }))

        let correctPosition: any = null
        let hasPosition: boolean = false

        let x = 0
        do {
            const node = { x: !sideOfRootNode ? checkRect.x : (checkRect.x + checkRect.width) - Configuration.nodeSize.width, y: checkRect.y + (nodeHeight * x), width: Configuration.nodeSize.width, height: 50 }
            
            const result = nodes.filter((p: IItemPosition) =>
                isCollide(node, { ...p, width: Configuration.nodeSize.width, height: 50 }))

            if (!result.length) {
                correctPosition = node
                hasPosition = true
            }

            x++

            if (x === 30) { 
                correctPosition = { x: 0, y: 0 }
                hasPosition = true
            }
        }
        while (hasPosition === false)

        return correctPosition
    }
}
