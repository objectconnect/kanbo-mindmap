import { Configuration } from '../../configuration'
import { generateFromTemplate } from '../../helpers/main'
import { Context } from '../../context'
import { Templates } from '../../templates'
import { IZoom, IZoomFactor, XY } from '../../constants/interfaces'
import { MovementHelper } from '../../helpers/movement'
import { MOVEMENT_DIRECTION } from '../../constants'
import { EVENT, reiseEvent } from '../../events'

/**
 * Mindmap zoom
 */
export class Zoom {

    /**
     * Global Context of control
     */
    private ctx: Context = Context.getInstance()

    /**
     * Columns HTML container
     */
    container: any

     /**
      * Current element
      */
    element: any

    /**
     * Zoom selector
     */
    selector: HTMLElement

    /**
     * Currently applied zoom factor
     */
    zoom: IZoom = { diff: {} as XY, factor: {} as IZoomFactor } as IZoom

    /**
     * Movement blackbox connection
     */
    movementInstance: any

    /**
     * Creates an instance of zoom.
     */
    public constructor() {

        this.ctx.zoom = this

        // Create control
        this.create()

        // Set max
        this.zoom.max = Configuration.maxZoom

        // Set min
        this.zoom.min = Configuration.minZoom

        // Set initial zoom value
        this.zoom.current = Configuration.zoom

        this.define()
    }

    /**
     * Create base structure and attach events
     */
    create = () => {

        // Genrate DOM structure
        this.element = generateFromTemplate(Templates.zoom)
        this.ctx.container.appendChild(this.element)
        
        // Load subcontrols
        this.container = this.element.children[0];
        this.selector = this.container.children[0];

        this.movementInstance = new MovementHelper(
            this.container, this.selector)
        
        this.movementInstance.set({
            actionCallback: this.onMovement,
            finishCallback: this.onMovementFinished,
            directAction: true,
            absoluteChange: true,
            direction: MOVEMENT_DIRECTION.VERTICAL
        })
    }

    /**
     * Calculate zoom difference
     */
    calculate = () => {
        const width = this.ctx.viewport.size.containerWidth;
        const height = this.ctx.viewport.size.containerHeight;
        
        this.zoom.diff.x = (width - (width * this.zoom.current)) / 2;
        this.zoom.diff.y = (height - (height * this.zoom.current)) / 2;
    }

    define = () => {
        this.zoom.factor.size = this.container.clientHeight - this.selector.clientHeight

        // Set max factor
        this.zoom.factor.max = this.zoom.factor.size * (this.zoom.max - 1)

        // Set scale factor
        this.zoom.factor.scale = (this.zoom.max - 1) + (1 - this.zoom.min)

        // Set max top
        this.zoom.factor.top = this.zoom.factor.max / this.zoom.factor.scale
        
        // Set initial zoom value
        this.selector.children[0]['innerText'] = "{0}x".format(
            parseFloat(this.zoom.current.toString()).toFixed(1))

        // Set initial top position
        this.selector.style.top = `${this.calculateSliderPosition()}px`
    }

    calculateSliderPosition = () => {
        const result = this.zoom.factor.size * (((this.zoom.current - this.zoom.min) / (this.zoom.max - this.zoom.min)))
        
        return result
    }

    update = (zoomValue: number, globalEvent = false) => {

        const { max, min } = this.zoom

        // Check new value
        if (zoomValue > max) zoomValue = this.zoom.max
        if (zoomValue < min) zoomValue = this.zoom.min

        if (globalEvent) {
            Configuration.zoom = zoomValue
            reiseEvent(EVENT.onZoomChanged, { zoom: zoomValue })
        }

        // Update current zoom value
        this.zoom.current = zoomValue

        this.define()
        this.calculate()

        this.ctx.refresh()
    }

    /**
     * On zoom movement
     */
    onMovement = (args) => {
        const { max, min } = this.zoom
        
        let newPosition = 1 + ((1 - max) * (1 - (args.position.y / this.zoom.factor.top)))

        if (newPosition > max) newPosition = this.zoom.max
        if (newPosition < min) newPosition = this.zoom.min

        this.zoom.current = parseFloat(newPosition.toFixed(2))

        Configuration.zoom = parseFloat(newPosition.toFixed(2))

        this.selector.children[0]['innerText'] = "{0}x".format(
            parseFloat(this.zoom.current.toString()).toFixed(1))

        this.calculate()

        this.ctx.refresh()
    }

    onMovementFinished = (args) => {

        // Set new config value
        Configuration.zoom = this.zoom.current

        // Reise global event
        reiseEvent(EVENT.onZoomChanged, { zoom: this.zoom.current })
    }
}
