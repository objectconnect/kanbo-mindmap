import { Configuration } from "../../configuration"
import { generateFromTemplate, isCollide } from '../../helpers/main'
import { Context } from '../../context'
import { Templates } from '../../templates'
import { IItemPosition, IScrollbars, IViewportSize, XY } from "../../constants/interfaces"
import { ScrollBar } from "../scrollbar"
import { MovementHelper } from "../../helpers/movement"
import { CALCULATION_MODE, SCROLLBAR_DIRECTION } from "../../constants"
import { reiseEvent, EVENT } from "../../events"

/**
 * Mindmap viewport
 */
export class Viewport {

    /**
     * Global Context of control
     */
    private ctx: Context = Context.getInstance()

    /**
     * Columns HTML container
     */
    container: HTMLElement

     /**
      * Current element
      */
    element: HTMLElement

    /**
     * Zoom selector
     */
    selector: HTMLElement

    /**
     * Scrollbars
    */
    scrollbars: IScrollbars = {} as IScrollbars
    
    /**
     * Current viewport size and position
     */
    size: IViewportSize = {} as IViewportSize

    /**
     * Movement blackbox connection
     */
    movementInstance: any

    hasMovement: boolean = false

    /**
     * Creates an instance of zoom.
     */
    public constructor() {

        this.ctx.viewport = this

        this.create()
    }

    create = () => {

        // Genrate DOM structure
        this.element = generateFromTemplate(Templates.viewport.format(
            Configuration.defaultContainerSize.width,
            Configuration.defaultContainerSize.height
        ))

        this.container = this.element.children[0] as HTMLElement
        this.container.style.transform = `scale(${Configuration.zoom})`

        this.ctx.container.appendChild(this.element)

        // Get current sizes
        this.getSize()

        // Set initial container position
        if (Configuration.viewportPosition) {
            this.container.style.top = `${Configuration.viewportPosition.y}px`
            this.container.style.left = `${Configuration.viewportPosition.x}px`
        } else {
            this.container.style.top = `${-((this.size.containerHeight - this.size.height) / 2)}px`
            this.container.style.left = `${-((this.size.containerWidth - this.size.width) / 2)}px`
        }

        // Calculate factor
        this.scrollbars.horizontalFactor = this.size.containerWidth / this.size.width
        this.scrollbars.verticalFactor = this.size.containerHeight / this.size.height

        // Calculate scrollbars size ( custom )
        const horizontalBarSize = this.size.width / this.scrollbars.horizontalFactor
        const verticalBarSize = this.size.height / this.scrollbars.verticalFactor

        // Initialize horizontal scrollbar
        this.scrollbars.horizontal = new ScrollBar(
            this.element,
            SCROLLBAR_DIRECTION.HORIZONTAL,
            horizontalBarSize,
            (this.size.width / 2) - (horizontalBarSize / 2),
            this.change
        )
        
        // Initialize vertical scrollbar
        this.scrollbars.vertical = new ScrollBar(
            this.element,
            SCROLLBAR_DIRECTION.VERTICAL,
            verticalBarSize, 
            (this.size.height / 2) - (verticalBarSize / 2),
            this.change
        )

        this.movementInstance = new MovementHelper(
            this.element, this.container)
        
        this.movementInstance.set({
            startCallback: this.onMovementStarted,
            actionCallback: this.onMovement,
            finishCallback: this.onMovementFinished,
            absoluteChange: true,
            directAction: true,
            useZoomByCalculation: true,
            calculationMode: CALCULATION_MODE.OUTSIDE
        })

        this.element.ondragover = this.onDragOver
        this.element.ondrop = this.onDrop
    }

    /**
     * Load current view size and position
     */
    getSize = () => {
        const boundingBox = this.ctx.container.getBoundingClientRect()
            
        this.size = {
            width: this.element.clientWidth,
            height: this.element.clientHeight,
            containerWidth: this.container.clientWidth,
            containerHeight: this.container.clientHeight,
            offsetTop: this.container.offsetTop,
            offsetLeft: this.container.offsetLeft,
            parentTop: boundingBox.top - 10,
            parentLeft: boundingBox.left
        }
    }

    resetPosition = () => {
        this.container.style.top = `${-((this.size.containerHeight - this.size.height) / 2)}px`
        this.container.style.left = `${-((this.size.containerWidth - this.size.width) / 2)}px`

        this.change({ x: 0, y:0 }, true, true, null, false)
    }

    change = (change, updateNavigator, updateScroll, newFactor, reiseGlobalEvent = true) => {
        
        if (!change) change = {} as XY

        this.scrollbars.horizontalFactor = (this.size.containerWidth * this.ctx.zoom.zoom.current) / this.size.width;
        this.scrollbars.verticalFactor = (this.size.containerHeight * this.ctx.zoom.zoom.current) / this.size.height;

        // TODO: how to optimize ? 
        var verticalFactor = newFactor && newFactor.vertical ? newFactor.vertical : this.scrollbars.verticalFactor;
        var horizontalFactor = newFactor && newFactor.horizontal ? newFactor.horizontal : this.scrollbars.horizontalFactor;
        var changeY = this.container.offsetTop - (change.y * verticalFactor);
        var changeX = this.container.offsetLeft - (change.x * horizontalFactor);

        if (this.container.clientHeight * this.ctx.zoom.zoom.current > this.size.height) {
            // Check if container doesn't go outside
            if (changeY + this.ctx.zoom.zoom.diff.y > 0) 
                this.container.style.top = `${0 - this.ctx.zoom.zoom.diff.y}px`

            else if (-(this.container.clientHeight - this.element.clientHeight) + this.ctx.zoom.zoom.diff.y > changeY)
                this.container.style.top = `${-(this.container.clientHeight - this.element.clientHeight) + this.ctx.zoom.zoom.diff.y}px`

            else 
                this.container.style.top = `${changeY}px`
        }

        if (this.container.clientWidth * this.ctx.zoom.zoom.current > this.size.width) {
            if ((changeX + this.ctx.zoom.zoom.diff.x > 0)) { 
                this.container.style.left = `${0 - this.ctx.zoom.zoom.diff.x}px`
            }
            
            else if ((-(this.container.clientWidth - this.element.clientWidth) + this.ctx.zoom.zoom.diff.x > changeX)) {
                this.container.style.left = `${-(this.container.clientWidth - this.element.clientWidth) + this.ctx.zoom.zoom.diff.x}px`
            }

            else
                this.container.style.left = `${changeX}px`
        }

        if (reiseGlobalEvent && (change.x !== 0 || change.y !== 0)) 
            reiseEvent(EVENT.onPositionChanged, { position: { x: this.container.offsetLeft, y: this.container.offsetTop }})

        Configuration.viewportPosition = {
            x: this.container.offsetLeft,
            y: this.container.offsetTop
        }

        if (updateNavigator)
            this.ctx.navigator.setNewPosition()

        if (updateScroll) {
            this.scrollbars.vertical.set(
                (this.container.offsetTop + this.ctx.zoom.zoom.diff.y) / this.scrollbars.verticalFactor)

            this.scrollbars.horizontal.set(
                (this.container.offsetLeft + this.ctx.zoom.zoom.diff.x) / this.scrollbars.horizontalFactor)
        }
    }

    onMovementStarted = (args) => {
        if (this.ctx.colorPicker) {            
            this.ctx.colorPicker.destroy()
            this.ctx.colorPicker = null

            return
        }

        if (this.ctx.menu.isVisible) {
            if (this.ctx.menu.current) {
                this.ctx.menu.current.element.classList.remove('selected')
                this.ctx.menu.current = null
            }
            this.ctx.menu.toggle()
        }
    }

    // Function called after user interaction with control
    // ( by movement -> by each change)
    onMovement = (args) => {
        this.hasMovement = true

        const width = this.ctx.viewport.size.containerWidth,
              height = this.ctx.viewport.size.containerHeight,
              xDiff = (width - (width * this.ctx.zoom.zoom.current)) / 2,
              yDiff = (height - (height * this.ctx.zoom.zoom.current)) / 2

        this.scrollbars.vertical.set(
            (this.container.offsetTop + yDiff) / this.scrollbars.verticalFactor);

        this.scrollbars.horizontal.set(
            (this.container.offsetLeft + xDiff) / this.scrollbars.horizontalFactor);

        this.ctx.navigator.setNewPosition()

        if (this.ctx.menu.isVisible) {
            if (this.ctx.menu.current) {
                this.ctx.menu.current.element.classList.remove('selected')
                this.ctx.menu.current = null
            }
            this.ctx.menu.toggle()
        }

        if (this.ctx.colorPicker) {
            this.ctx.colorPicker.destroy()
            this.ctx.colorPicker = null
        }
    }
    
    onMovementFinished = (args) => {
        if (!this.hasMovement) return

        this.hasMovement = false
        const position = {
            x: this.container.offsetLeft,
            y: this.container.offsetTop
        }

        Configuration.viewportPosition = position

        reiseEvent(EVENT.onPositionChanged, { position })
    }

    onDragOver = (args) => {
        args.preventDefault()
        args.stopPropagation()
    }
    
    onDrop = (args: DragEvent) => {
        args.preventDefault()
        args.stopPropagation()

        const boundingRect = this.container.getBoundingClientRect()

        const restZoom = 1 / this.ctx.zoom.zoom.current

        // Problem - about `100px ( viewport parent ?)
        let x = ((args.clientX + Math.abs(boundingRect.x)) * restZoom) - this.size.parentLeft
        let y = ((args.clientY + Math.abs(boundingRect.y)) * restZoom)

        const nodesCollisions = Object.values(this.ctx.positions).filter((p: IItemPosition) =>
            isCollide(
                { x, y, width: Configuration.nodeSize.width, height: 50 },
                { ...p, width: Configuration.nodeSize.width, height: 50 }))

        const rootCollision = isCollide({
            ...this.ctx.renderedElements.root.data.position,
            ...Configuration.rootNode.size
        }, { x, y, width: Configuration.nodeSize.width, height: 50 })

        if (rootCollision || nodesCollisions.length) {
            const newPosition = this.ctx.nodes.findNewPosition({ x, y })

            x = newPosition.x
            y = newPosition.y
        }            

        reiseEvent(EVENT.onItemDropped, { x, y });
    }
}
