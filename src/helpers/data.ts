import { Context } from "../context"
import { Configuration } from "../configuration"
import { IConfiguration, Index, IRootNode } from "../constants/interfaces"
import { isNull } from "./main"

let ctx: Context

export function initializeContext() { ctx = Context.getInstance() }

export function updateConfiguration(config: Partial<IConfiguration>, initial?: boolean) {
    let reload = false
    
    for (const update in config) {

        const key = update as keyof any,
              value = config[key.toString()]
        
        if (key === 'rootNode') {
            updateRootNodeConfig(value)
            ctx.refresh()
            continue
        }
        
        if (key === 'items') {
            createIndexes(value)
            createConnections()
        }

        if (key === 'connections') createConnections(value)
        
        if (key === 'nodeSize' && value && (!value.height || value.height < 3))
            value.height = 3

        if (key === 'viewportPosition') {
            updateViewportPosition(value)
            continue
        }

        if (!initial && key === "locale" && (value && Configuration[key] !== value))
            setLocale(value)

        if (key === 'zoom' && (!isNull(value) && Configuration[key] !== value))
            updateZoom(value)

        if (!isNull(value) && Configuration[key] !== value) {
            if (Configuration.requiredReload.indexOf(key.toString()) !== -1)
                reload = true;

            (Configuration[key] as any) = value;
        }
    }
    
    if (reload) {
        //updateIndexes()
        
        !initial && ctx.reload()
        return
    }

    ctx.refresh()
}

export function setLocale(locale: string) {

    if (!ctx.localization) return

    ctx.localization.set(locale).then(() => ctx.reload(true))
}

export function updateZoom(zoomValue: number) {
    
    if (!ctx.zoom) return
    
    ctx.zoom.update(zoomValue)
}

export function updateRootNodeConfig(config: Partial<IRootNode>) {
    for (const update in config) {
        
        const key = update as keyof any,
              value = config[key.toString()]

        Configuration.rootNode[key] = value
    }
}

export function updateViewportPosition(position) {
    if (!ctx.viewport) {
        
        // If viewport doesn't exists update only configuration
        Configuration.viewportPosition = position

        // Break process here
        return
    }
    
    if (position && (JSON.stringify(position) !== JSON.stringify(Configuration.viewportPosition))) {
        
        ctx.viewport.container.style.left = `${position.x}px`,
        ctx.viewport.container.style.top = `${position.y}px`
        
        //ctx.navigator.setNewPosition()

        //ctx.viewport.scrollbars.vertical.set(
        //    (ctx.viewport.container.offsetTop + ctx.zoom.zoom.diff.y) / ctx.viewport.scrollbars.verticalFactor)

        //ctx.viewport.scrollbars.horizontal.set(
        //    (ctx.viewport.container.offsetLeft + ctx.zoom.zoom.diff.x) / ctx.viewport.scrollbars.horizontalFactor)
        
        //ctx.viewport.change(change, true, true, null, false)
        ctx.viewport.change({ x: 0, y: 0 }, true, true, null, false)
    } else if (!position)
        ctx.viewport.resetPosition()
}

export function createIndexes(data?: any[]) {
    const result = {};
    let items: any[] = []

    ctx.positions = []

    if (data) {
        items = data
        ctx.data.originalItems = data
    } else items = ctx.data.originalItems

    items.forEach(e => {
        
        // Create new index in collection
        const index = <Index>{} // Convert to type
        
        // Clone input object
        index.data = Object.assign({}, e)

        // Position of node is on its bottom center
        index.position = { x: e.x - (Configuration.nodeSize.width / 2) , y: e.y }

        index.id = e.id
        index.css = []

        index.canMove = true

        index.children = []
        
        result[e.id] = index

        // Store position in global context
        ctx.positions[e.id] = { id: e.id, ...index.position }
    })

    
    ctx.itemsCount = items.length
    ctx.hasContent = true

    ctx.data.nodes = result
}

export function updateIndexes() {
    createIndexes()    
    createConnections()
}

export function createConnections(data?: any[]) {
    let connections: any[], oldConnections: any[]

    if (data) {
        ctx.data.originalConnections = data

        connections = data
    } else
        connections = ctx.data.originalConnections

    connections.map(connection => {
        if (isNull(connection.from)) return

        const id = `_${connection.from}_${connection.to}_`
        const exists = ctx.renderedElements.connections[id]

        if (exists) delete ctx.renderedElements.connections[id]

        const node = ctx.data.nodes[connection.from]

        if (!node) return

        if (!isNull(connection.to)) {
            node.parentId = connection.to
            
            if (ctx.data.nodes[connection.to] &&
                ctx.data.nodes[connection.to].children &&
                ctx.data.nodes[connection.to].children.indexOf(connection.from) === -1)
                ctx.data.nodes[connection.to].children.push(connection.from)
            else if (ctx.data.nodes[connection.to] && !ctx.data.nodes[connection.to].children)
                ctx.data.nodes[connection.to].children = [connection.from]
        } else node.parentId = -1

        if (!isNull(connection.color))
            node.connectionColor = connection.color
    })

    Object.keys(ctx.renderedElements.connections).map(c => {
        if (c.indexOf('-1') !== -1) return

        const node = ctx.data.nodes[ctx.renderedElements.connections[c]]

        if (node) {
            node.parentId = -1
        }

        delete ctx.renderedElements.connections[c]
    })
}
