import { CALCULATION_MODE, MOVEMENT_DIRECTION } from "../constants"
import { Configuration } from "../configuration"
import { Interaction, XY } from "../constants/interfaces"
import { Context } from "../context"
import { calculateInside, calculateOutside, eventPath, uuidv4 } from "./main"

/**
 * Class for managing all user interactions
 */
export class MovementBlackBox {
    
    /**
     * Global Context of control
     */
    ctx: Context = Context.getInstance()

    /**
     * Controls alowing interactions
     */
    instances: any = {}
    
    /**
     * Current interaction target
     */
    target: any = null

    zoomValue: number

    hasInteraction: boolean = false
    interactionTarget: any = null

    /**
     * Creates instance of movement black box
     */
    constructor() {
        
        // Attach to mouse wheel event
        this.ctx.container.addEventListener(
            'wheel', this.onMouseWheel, { passive: false })
        this.ctx.container.addEventListener(
            'mousewheel', this.onMouseWheel, { passive: false })
        this.ctx.container.addEventListener(
            'DOMMouseScroll', this.onMouseWheel, { passive: false })

        // Attach to other browser events
        document.addEventListener('keydown', this.onKeyDown, { passive: false })
        document.addEventListener('keyup', this.onKeyUp, { passive: false })
        document.addEventListener('mousemove', this.onMouseMove, { passive: false })
        document.addEventListener('mouseup', this.onMouseUp, { passive: false })
        document.addEventListener('touchmove', this.onTouchMove, { passive: false })
        document.addEventListener('touchend', this.onTouchEnd, { passive: false })
        document.addEventListener('touchcancel', this.onTouchCancel, { passive: false })
    }

    /**
     * Supported keyboard kays
     */
    supportedKeys = {
        ArrowUp: {
            keyDown: () => {
                this.ctx.viewport.change({ x: 0, y: -3 }, true, true, null, true)}},
        ArrowDown: {
            keyDown: () => {
                this.ctx.viewport.change({ x: 0, y: 3 }, true, true, null, true)}},
        ArrowLeft: {
            keyDown: () => {
                this.ctx.viewport.change({ x: -3, y:0 }, true, true, null, true)}},
        ArrowRight: {
            keyDown: () => {
                this.ctx.viewport.change({ x: 3, y:0 }, true, true, null, true)}}
    }

    onKeyDown = (args) => {
        const key = this.supportedKeys[args.key];
        if (key && key.keyDown && document.URL.toLowerCase().indexOf('card') === -1) key.keyDown();
    }

    onKeyUp = (args) => {
        const key = this.supportedKeys[args.key];
        if (key && key.keyDown && document.URL.toLowerCase().indexOf('card') === -1) key.keyDown();
    }

    onMouseMove = (args) => {
        if (!this.target) return;
        
        this.instances[this.target].onMouseMove(args);
    }

    onMouseUp = (args) => {
        if (!this.target) return;

        this.instances[this.target].onMouseUp(args);
    }

    onMouseWheel = (args) => {

        if (args.ctrlKey) {
            args.preventDefault();
            
            this.ctx.zoom.update(this.ctx.zoom.zoom.current + (args.deltaY / 1000), true);
        } else
            this.ctx.viewport.change({ x: (args.deltaX / 50), y: (args.deltaY / 50) }, true, true, null, true)
    }

    onTouchMove = (args) => {
        if (!this.target) return;

        this.instances[this.target].onMouseMove(args);
    }

    onTouchEnd = (args) => {
        if (!this.target) return;

        this.instances[this.target].onMouseUp(args);
    }

    onTouchCancel = (args) => {
        if (!this.target) return;

        this.instances[this.target].onMouseUp(args);
    }

    dispose = () => {
        this.ctx.container.removeEventListener('wheel', this.onMouseWheel);
        this.ctx.container.removeEventListener('mousewheel', this.onMouseWheel);
        this.ctx.container.removeEventListener('DOMMouseScroll', this.onMouseWheel);   

        document.removeEventListener('keydown', this.onKeyDown)
        document.removeEventListener('keyup', this.onKeyUp)
        
        document.removeEventListener('mouseup', this.onMouseUp)
        document.removeEventListener('mousemove', this.onMouseMove)
        
        document.removeEventListener('touchmove', this.onTouchMove)
        document.removeEventListener('touchend', this.onTouchEnd)
        document.removeEventListener('touchcancel', this.onTouchCancel)
    }
}

export class MovementHelper {
    
    /**
     * Global Context of control
     */
    ctx: Context = Context.getInstance()

    id: string = uuidv4()

    hasUserInteraction: boolean
    passTrought: boolean
    attachToElement: boolean

    startPosition: XY = <XY>{}
    lastChange: XY = <XY>{ x: 0, y: 0 }
    lastMousePosition: XY = <XY>{}

    container: any
    element: any
    css: string

    startCallback: any
    actionCallback: any
    finishCallback: any

    useCustomCellSize: boolean
    customCellSize: any

    useZoomByCalculation: boolean

    directAction: boolean
    absoluteChange: boolean

    direction: MOVEMENT_DIRECTION = MOVEMENT_DIRECTION.BOTH
    calculationMode: CALCULATION_MODE = CALCULATION_MODE.INSIDE

    constructor(
        container: any,
        element: any,
        attachToElement: boolean = false
    ) {
        this.ctx.movementBlackBox.instances[this.id] = this

        this.container = container
        this.element = element

        this.attachToElement = attachToElement

        this.attach()
    }

    attach = () => {
        if (this.attachToElement) {
            this.element.onmousedown = this.onMouseDown
            this.element.ontouchdown = this.onMouseDown
            return;
        }

        this.container.onmousedown = this.onMouseDown
        this.container.ontouchdown = this.onMouseDown
    }

    onMouseDown = (args) => {

        if(this.ctx.movementBlackBox.hasInteraction) return

        this.ctx.movementBlackBox.hasInteraction = true
        this.ctx.movementBlackBox.interactionTarget = this.id

        if (this.passTrought)
            this.passTrought = false;

        // Get current interaction ( mouse / touch ) position
        var [x, y] = this.getInteractionCoordinates(args);

        // Set current interaction target
        this.ctx.movementBlackBox.target = this.id;
        
        // Unblock interaction events
        this.hasUserInteraction = true;

        // Set new start position
        this.startPosition = { x, y };

        // Set custom styling for current interaction target
        if (this.css) this.element.classList.add(this.css)

        // Make callback to parent component
        if (this.startCallback) this.startCallback(args);
    }

    onMouseMove = (args) => {

        if (this.ctx.movementBlackBox.interactionTarget !== this.id) return

        // If no user interaction ( clear mouse movement )
        if (!this.hasUserInteraction && !this.passTrought) return;

        // If no button used...
        if (!args.buttons && !args.touches  && !this.passTrought) {
            this.onMouseUp(args);
            return;
        }

        args.stopPropagation();
        if (args.defaultPrevented)
            args.preventDefault();

        // Calculate user interaction change
        const grid = this.calculate(args);
        
        if (this.directAction)            
            switch(this.calculationMode) {
                case CALCULATION_MODE.INSIDE:
                    calculateInside(
                        grid.position,
                        this.container,
                        this.element,
                        this.direction)
                    break
                
                case CALCULATION_MODE.OUTSIDE:
                    calculateOutside(
                        grid.position,
                        this.element,
                        this.container,
                        this.ctx.zoom.zoom.current,
                        this.useZoomByCalculation)
                    break
            }

        // Make callback to parent component
        if (this.actionCallback) this.actionCallback(grid);
    }

    onMouseUp = (args) => {

        if (!this.ctx.movementBlackBox.hasInteraction || this.ctx.movementBlackBox.interactionTarget !== this.id) return
        
        this.ctx.movementBlackBox.interactionTarget = null
        this.ctx.movementBlackBox.hasInteraction = false
        
        if (this.hasUserInteraction) {
            this.hasUserInteraction = false;

            // Reset global movement target
            this.ctx.movementBlackBox.target = null;

            if (this.css) this.element.classList.remove(this.css);

            if (this.finishCallback) this.finishCallback({
                default: args,
                startPosition: this.startPosition
            })

            // Reset events data
            this.startPosition = <XY>{};
            this.lastChange = <XY>{ x: 0, y: 0 };
            this.lastMousePosition = <XY>{};
        }
    }

    calculate = (args) => {
        if (!this.lastMousePosition) this.lastMousePosition = this.startPosition;

        const [x, y] = this.getInteractionCoordinates(args);
        
        const width = this.useCustomCellSize ?
            this.customCellSize.width : Configuration.cell.width

        const height = this.useCustomCellSize ?
            this.customCellSize.height : Configuration.cell.height
        
        const zoomValue = this.calculationMode !== CALCULATION_MODE.OUTSIDE && this.useZoomByCalculation ? this.ctx.zoom.zoom.current : 1

        const result = <Interaction>{}
        const change =
            !this.absoluteChange ? {
                x: Math.round(Math.abs((this.startPosition.x - x) / width)),
                y: Math.round(Math.abs((this.startPosition.y - y) / height))
            } : {
                x: (x - this.startPosition.x) / zoomValue,
                y: (y - this.startPosition.y) / zoomValue,
            }
                
        result.startPosition = this.startPosition
        result.path = eventPath(args)
        result.default = args
        result.mouse = { x, y }

        if (!this.absoluteChange)
            result.change = {
                x: Math.abs(change.x - this.lastChange.x),
                y: Math.abs(change.y - this.lastChange.y)
            };
        else result.change = {
            x: change.x - this.lastChange.x,
            y: change.y - this.lastChange.y
        };
        
        result.direction = {
            x: this.lastMousePosition.x > x ? 1 : 0,
            y: this.lastMousePosition.y > y ? 1 : 0
        };

        if (this.directAction)
            result.position = {
                x: this.element.offsetLeft + result.change.x,
                y: this.element.offsetTop + result.change.y
            }

        this.lastMousePosition = { x, y }
        this.lastChange = change

        return result;
    }

    getInteractionCoordinates = (args) => {
        const x = !args.clientX && args.touches && args.touches.length
                        ? args.touches[0].clientX : args.clientX
        const y = !args.clientY && args.touches && args.touches.length
                        ? args.touches[0].clientY : args.clientY

        return [x, y];
    }

    set = (properties: Partial<any>) => {
        for (const update in properties) {
            const key = update as keyof any;
            (this[key] as any) = properties[key.toString()];
        }
    }
}