export enum SCROLLBAR_DIRECTION {
    HORIZONTAL = 0,
    VERTICAL = 1
}

export enum MOVEMENT_DIRECTION {    
    HORIZONTAL = 0,
    VERTICAL = 1,
    BOTH = 3
}

export enum CALCULATION_MODE {
    INSIDE = 0,
    OUTSIDE = 1
}

export enum COLOR_PICKER_TARGET {
    ROOT_NODE = 0,
    CONNECTION = 1
}