const importLocale = (lang) => import(`./${lang}.json`);
export let strings = {};
export class Localization {
    constructor() {
        // Currently used language
        this.locale = 'en';
    }
    async set(locale) {
        return new Promise((resolve, reject) => {
            if (!locale) {
                importLocale(locale).then((result) => {
                    this.strings = result;
                    strings = this.strings;
                    this.locale = locale;
                    resolve(null);
                });
                return;
            }
            this.strings = importLocale(locale).then((result) => {
                this.strings = result;
                strings = this.strings;
                this.locale = locale;
                resolve(null);
            });
        });
    }
}
//# sourceMappingURL=index.js.map