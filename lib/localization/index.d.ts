export declare let strings: any;
export declare class Localization {
    locale: string;
    strings: any;
    constructor();
    set(locale: string): Promise<unknown>;
}
