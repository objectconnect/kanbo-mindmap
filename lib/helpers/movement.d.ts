import { CALCULATION_MODE, MOVEMENT_DIRECTION } from "../constants";
import { Interaction, XY } from "../constants/interfaces";
import { Context } from "../context";
/**
 * Class for managing all user interactions
 */
export declare class MovementBlackBox {
    /**
     * Global Context of control
     */
    ctx: Context;
    /**
     * Controls alowing interactions
     */
    instances: any;
    /**
     * Current interaction target
     */
    target: any;
    zoomValue: number;
    hasInteraction: boolean;
    interactionTarget: any;
    /**
     * Creates instance of movement black box
     */
    constructor();
    /**
     * Supported keyboard kays
     */
    supportedKeys: {
        ArrowUp: {
            keyDown: () => void;
        };
        ArrowDown: {
            keyDown: () => void;
        };
        ArrowLeft: {
            keyDown: () => void;
        };
        ArrowRight: {
            keyDown: () => void;
        };
    };
    onKeyDown: (args: any) => void;
    onKeyUp: (args: any) => void;
    onMouseMove: (args: any) => void;
    onMouseUp: (args: any) => void;
    onMouseWheel: (args: any) => void;
    onTouchMove: (args: any) => void;
    onTouchEnd: (args: any) => void;
    onTouchCancel: (args: any) => void;
    dispose: () => void;
}
export declare class MovementHelper {
    /**
     * Global Context of control
     */
    ctx: Context;
    id: string;
    hasUserInteraction: boolean;
    passTrought: boolean;
    attachToElement: boolean;
    startPosition: XY;
    lastChange: XY;
    lastMousePosition: XY;
    container: any;
    element: any;
    css: string;
    startCallback: any;
    actionCallback: any;
    finishCallback: any;
    useCustomCellSize: boolean;
    customCellSize: any;
    useZoomByCalculation: boolean;
    directAction: boolean;
    absoluteChange: boolean;
    direction: MOVEMENT_DIRECTION;
    calculationMode: CALCULATION_MODE;
    constructor(container: any, element: any, attachToElement?: boolean);
    attach: () => void;
    onMouseDown: (args: any) => void;
    onMouseMove: (args: any) => void;
    onMouseUp: (args: any) => void;
    calculate: (args: any) => Interaction;
    getInteractionCoordinates: (args: any) => any[];
    set: (properties: Partial<any>) => void;
}
