import { MOVEMENT_DIRECTION } from "../constants";
export function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
export function eventPath(evt) {
    const path = (evt.composedPath && evt.composedPath()) || evt.path, target = evt.target;
    if (path != null) {
        // Safari doesn't include Window, but it should.
        return (path.indexOf(window) < 0) ? path.concat(window) : path;
    }
    if (target === window) {
        return [window];
    }
    function getParents(node, memo) {
        memo = memo || [];
        const parentNode = node.parentNode;
        if (!parentNode) {
            return memo;
        }
        else {
            return getParents(parentNode, memo.concat(parentNode));
        }
    }
    return [target].concat(getParents(target), window);
}
export function isNull(value) {
    return value == null || value == undefined;
}
export function generateFromTemplate(template) {
    var wrapper = document.createElement('TemplateWrapper');
    wrapper.innerHTML = template;
    return wrapper.childNodes[0];
}
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match;
        });
    };
}
export function calculateInside(pos, cont, elm, dir = MOVEMENT_DIRECTION.BOTH) {
    switch (dir) {
        case MOVEMENT_DIRECTION.HORIZONTAL:
            setHorizontalPosition(pos, elm, cont);
            break;
        case MOVEMENT_DIRECTION.VERTICAL:
            setVerticalPosition(pos, elm, cont);
            break;
        default: // BOTH
            setHorizontalPosition(pos, elm, cont);
            setVerticalPosition(pos, elm, cont);
            break;
    }
}
export function setHorizontalPosition(pos, elm, cont) {
    if (pos.x < 0)
        elm.style.left = `0px`;
    else if (pos.x + elm.clientWidth > cont.clientWidth)
        elm.style.left = `${cont.clientWidth - elm.clientWidth}px`;
    else
        elm.style.left = `${pos.x}px`;
}
export function setVerticalPosition(pos, elm, cont) {
    if (pos.y < 0)
        elm.style.top = `0px`;
    else if (pos.y + elm.clientHeight > cont.clientHeight)
        elm.style.top = `${cont.clientHeight - elm.clientHeight}px`;
    else
        elm.style.top = `${pos.y}px`;
}
export function calculateOutside(pos, elm, cont, zoom = 1, useZoom) {
    const sizeDiff = {
        x: useZoom ? (elm.clientWidth - (elm.clientWidth * zoom)) / 2 : 0,
        y: useZoom ? (elm.clientHeight - (elm.clientHeight * zoom)) / 2 : 0
    };
    if (elm.clientHeight * zoom > cont.clientHeight) {
        if (pos.y + sizeDiff.y > 0)
            elm.style.top = `${0 - sizeDiff.y}px`;
        else if (-(elm.clientHeight - cont.clientHeight) + sizeDiff.y > pos.y)
            elm.style.top = `${-(elm.clientHeight - cont.clientHeight) + sizeDiff.y}px`;
        else
            elm.style.top = `${pos.y}px`;
    }
    if (elm.clientWidth * zoom > cont.clientWidth) {
        if (pos.x + sizeDiff.x > 0)
            elm.style.left = `${0 - sizeDiff.x}px`;
        else if (-(elm.clientWidth - cont.clientWidth) + sizeDiff.x > pos.x)
            elm.style.left = `${-(elm.clientWidth - cont.clientWidth) + sizeDiff.x}px`;
        else
            elm.style.left = `${pos.x}px`;
    }
}
export function isCollide(rect1, rect2) {
    return (rect1.x < rect2.x + rect2.width &&
        rect1.x + rect1.width > rect2.x &&
        rect1.y < rect2.y + rect2.height &&
        rect1.height + rect1.y > rect2.y);
}
//# sourceMappingURL=main.js.map