export declare function drawCircle(ctx: any, x: any, y: any, radius?: number): void;
export declare function drawLine(ctx: any, start: any, end: any): void;
