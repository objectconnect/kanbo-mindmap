export function drawCircle(ctx, x, y, radius = 1) {
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
    ctx.fillStyle = 'green';
    ctx.fill;
    ctx.lineWidth = 1;
    ctx.strokeStyle = 'gray';
    ctx.stroke();
}
export function drawLine(ctx, start, end) {
    ctx.beginPath();
    ctx.moveTo(start.x, start.y);
    ctx.lineTo(end.x, end.y);
    ctx.strokeStyle = 'lightgray';
    ctx.stroke();
}
//# sourceMappingURL=canvas.js.map