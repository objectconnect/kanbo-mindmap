import { MOVEMENT_DIRECTION } from "../constants";
import { XY } from "../constants/interfaces";
export declare function uuidv4(): string;
export declare function eventPath(evt: any): any;
export declare function isNull(value: any): boolean;
export declare function generateFromTemplate(template: any): HTMLElement;
declare global {
    interface String {
        format(...args: any[]): string;
    }
}
export declare function calculateInside(pos: XY, cont: any, elm: any, dir?: MOVEMENT_DIRECTION): void;
export declare function setHorizontalPosition(pos: XY, elm: any, cont: any): void;
export declare function setVerticalPosition(pos: XY, elm: any, cont: any): void;
export declare function calculateOutside(pos: XY, elm: any, cont: any, zoom?: number, useZoom?: boolean): void;
export declare function isCollide(rect1: any, rect2: any): boolean;
