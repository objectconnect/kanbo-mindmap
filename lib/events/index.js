import { Context } from '../context';
export var EVENT;
(function (EVENT) {
    /**
     * Request to add new item
     */
    EVENT["onNewItem"] = "MindMap.onNewItem";
    /**
     * Request to remove item
     */
    EVENT["onItemDeleted"] = "MindMap.onItemDeleted";
    /**
     * After item is added to view
     */
    EVENT["onItemRender"] = "MindMap.onItemRender";
    /**
     * When item dropped on viewport
     */
    EVENT["onItemDropped"] = "MindMap.onItemDropped";
    /**
     * After item removed from view
     */
    EVENT["onItemDestroy"] = "MindMap.onItemDestroy";
    /**
     * After new connection ( line ) was created
     */
    EVENT["onNewConnection"] = "MindMap.onNewConnection";
    /**
     * After connection was deleted
     */
    EVENT["onConnectionDeleted"] = "MindMap.onConnectionDeleted";
    /**
     * After color was changed
     */
    EVENT["onColorChanged"] = "MindMap.onColorChanged";
    /**
     * When color picker is rendered
     */
    EVENT["onColorPickerRender"] = "MindMap.onColorPickerRender";
    /**
     * When color picker is removed from view
     */
    EVENT["onColorPickerDestroy"] = "MindMap.onColorPickerDestroy";
    /**
     * Change in configuration on control side
     */
    EVENT["onViewChanged"] = "MindMap.onViewChanged";
    /**
     * Zoom changed
     */
    EVENT["onZoomChanged"] = "MindMap.onZoomChanged";
    /**
     * Viewport position changed
     */
    EVENT["onPositionChanged"] = "MindMap.onPositionChanged";
    /**
     * After all items were removed from view at once
     */
    EVENT["onClean"] = "MindMap.onClean";
})(EVENT || (EVENT = {}));
export function reiseEventAsync(event, args) {
    const ctx = Context.getInstance();
    if (!ctx.container)
        return;
    const asyncEvent = new CustomEvent(event, {
        detail: args,
        bubbles: true,
        cancelable: true
    });
    setTimeout(() => {
        ctx.container.dispatchEvent(asyncEvent);
    }, 0);
}
export function reiseEvent(event, args) {
    const ctx = Context.getInstance();
    if (!ctx.container)
        return;
    ctx.container.dispatchEvent(new CustomEvent(event, {
        detail: args,
        bubbles: true,
        cancelable: true
    }));
}
//# sourceMappingURL=index.js.map