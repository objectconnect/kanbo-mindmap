export declare enum EVENT {
    /**
     * Request to add new item
     */
    onNewItem = "MindMap.onNewItem",
    /**
     * Request to remove item
     */
    onItemDeleted = "MindMap.onItemDeleted",
    /**
     * After item is added to view
     */
    onItemRender = "MindMap.onItemRender",
    /**
     * When item dropped on viewport
     */
    onItemDropped = "MindMap.onItemDropped",
    /**
     * After item removed from view
     */
    onItemDestroy = "MindMap.onItemDestroy",
    /**
     * After new connection ( line ) was created
     */
    onNewConnection = "MindMap.onNewConnection",
    /**
     * After connection was deleted
     */
    onConnectionDeleted = "MindMap.onConnectionDeleted",
    /**
     * After color was changed
     */
    onColorChanged = "MindMap.onColorChanged",
    /**
     * When color picker is rendered
     */
    onColorPickerRender = "MindMap.onColorPickerRender",
    /**
     * When color picker is removed from view
     */
    onColorPickerDestroy = "MindMap.onColorPickerDestroy",
    /**
     * Change in configuration on control side
     */
    onViewChanged = "MindMap.onViewChanged",
    /**
     * Zoom changed
     */
    onZoomChanged = "MindMap.onZoomChanged",
    /**
     * Viewport position changed
     */
    onPositionChanged = "MindMap.onPositionChanged",
    /**
     * After all items were removed from view at once
     */
    onClean = "MindMap.onClean"
}
export declare function reiseEventAsync(event: EVENT, args: any): void;
export declare function reiseEvent(event: EVENT, args: any): void;
