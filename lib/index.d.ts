import './styles/index.css';
import { IConfiguration } from './constants/interfaces';
import './assets/icons/16-plus.svg';
import './assets/icons/16-change-color.svg';
/**
 * Mindmap Control v1
 */
export default class MindMap {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Global Context of control
     */
    constructor(container: HTMLElement, initialConfig?: Partial<IConfiguration>);
    private initialize;
    /**
     * Set data or configuration
     * @param config Set of props to set
     */
    set: (config: Partial<IConfiguration>) => void;
    /**
     * Cause refresh of control
     */
    refresh: () => void;
    /**
     * Cause full reload of control
     */
    reload: () => void;
    /**
     * Dispose control and remove all components
     */
    dispose: () => void;
    destroyColorPicker: () => void;
}
export * from './constants';
export * from './constants/interfaces';
