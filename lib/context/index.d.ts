/**
 * MindMap control global context
 */
export declare class Context {
    /**
     * Instance  of context ( global )
     */
    private static instance?;
    /**
     * Creates an instance of context ( not used )
     */
    constructor();
    /**
     * Get current context instance
     * @returns instance
     */
    static getInstance: () => Context;
    /**
     * Removes instance and close singlenton
     */
    static removeInstance: () => void;
    /**
     * If control is already initialized
     */
    initialized: boolean;
    /**
     * If control has any content
     */
    hasContent: boolean;
    /**
     * Root view of control
     */
    viewport: any;
    /**
     * View navigator
     */
    navigator: any;
    /**
     * Zoom control
     */
    zoom: any;
    /**
     * Nodes control
     */
    nodes: any;
    /**
     * Connections control
     */
    connections: any;
    /**
     * Positions of nodes in midnmap
     */
    positions: any;
    /**
     * Context Menu
     */
    menu: any;
    /**
     * Current data
     */
    data: any;
    mapping: any;
    renderedElements: any;
    hasUpdate: boolean;
    currentUpdate: any;
    /**
     * Items rendered/available in current view
     */
    itemsCount: number;
    /**
     * Root control container
     */
    container: any;
    /**
     * Blackbox for user interactions
     */
    movementBlackBox: any;
    colorPicker: any;
    localization: any;
    /**
     * Refresh control
     */
    refresh: () => void;
    /**
     * Reload whole control
     */
    reload: (updateLocale?: boolean | undefined) => void;
    /**
     * Set resize event
    */
    attachWindowResize: () => void;
    /**
     * Dispose control and remove all elements
     */
    dispose: () => void;
}
