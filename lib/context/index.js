var _a;
import { EVENT, reiseEvent } from '../events';
import { updateIndexes } from '../helpers/data';
/**
 * MindMap control global context
 */
export class Context {
    /**
     * Creates an instance of context ( not used )
     */
    constructor() {
        /**
         * Current data
         */
        this.data = {
            nodes: {},
            originalItems: [],
            connections: [],
            originalConnections: []
        };
        this.mapping = {};
        // Current rendered data
        this.renderedElements = {
            root: null,
            nodes: {},
            connections: {}
        };
        this.hasUpdate = false;
        this.currentUpdate = {};
        /**
         * Items rendered/available in current view
         */
        this.itemsCount = 0;
        /**
         * Refresh control
         */
        this.refresh = () => {
            if (!this.viewport)
                return;
            this.viewport.container.style.transform = `scale(${this.zoom.zoom.current})`;
            this.viewport.scrollbars.horizontal.update();
            this.viewport.scrollbars.vertical.update();
            this.navigator.updateSelector();
            this.viewport.change({ x: 0, y: 0 }, true, true, null, false);
            this.nodes.render();
        };
        /**
         * Reload whole control
         */
        this.reload = (updateLocale) => {
            reiseEvent(EVENT.onClean, {});
            if (!this.viewport)
                return;
            this.viewport.container.style.transform = `scale(${this.zoom.zoom.current})`;
            this.viewport.getSize();
            this.viewport.scrollbars.horizontal.update();
            this.viewport.scrollbars.vertical.update();
            this.navigator.updateSelector();
            this.viewport.change({ x: 0, y: 0 }, true, true, null, false);
            if (updateLocale) {
                this.menu.recreate();
            }
            // Update control data
            updateIndexes();
            // Clear nodes/connections layer
            this.nodes.clear();
            // Rerender data
            this.nodes.render();
        };
        /**
         * Set resize event
        */
        this.attachWindowResize = () => {
            // Handle window resizing
            window.onresize = () => this.reload();
        };
        /**
         * Dispose control and remove all elements
         */
        this.dispose = () => {
            // Remove resize event
            window.onresize = null;
            // Deatach events
            this.movementBlackBox.dispose();
            // Remove root container
            this.container.remove();
            // Close context
            Context.removeInstance();
        };
    }
}
_a = Context;
/**
 * Get current context instance
 * @returns instance
 */
Context.getInstance = () => {
    if (!Context.instance)
        Context.instance = new Context();
    return Context.instance;
};
/**
 * Removes instance and close singlenton
 */
Context.removeInstance = () => {
    if (_a.instance)
        _a.instance = undefined;
};
//# sourceMappingURL=index.js.map