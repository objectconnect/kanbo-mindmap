export declare enum SCROLLBAR_DIRECTION {
    HORIZONTAL = 0,
    VERTICAL = 1
}
export declare enum MOVEMENT_DIRECTION {
    HORIZONTAL = 0,
    VERTICAL = 1,
    BOTH = 3
}
export declare enum CALCULATION_MODE {
    INSIDE = 0,
    OUTSIDE = 1
}
export declare enum COLOR_PICKER_TARGET {
    ROOT_NODE = 0,
    CONNECTION = 1
}
