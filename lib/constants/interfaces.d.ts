import { ScrollBar } from "components/scrollbar";
export interface XY {
    x: number;
    y: number;
}
export interface Interaction {
    startPosition: XY;
    mouse: XY;
    change: XY;
    direction: XY;
    path: string;
    default: any;
    position: XY;
}
export interface IScrollbars {
    horizontal: ScrollBar;
    horizontalFactor: number;
    vertical: ScrollBar;
    verticalFactor: number;
}
export interface IScollbar {
    viewSize: number;
    viewPosition: number;
    visibleSize: number;
}
export interface IViewportSize {
    width: number;
    height: number;
    containerWidth: number;
    containerHeight: number;
    offsetTop: number;
    offsetLeft: number;
    parentTop: number;
    parentLeft: number;
}
export interface IZoom {
    current: number;
    min: number;
    max: number;
    diff: XY;
    factor: IZoomFactor;
}
export interface IZoomFactor {
    max: number;
    min: number;
    scale: number;
    size: number;
    top: number;
}
export interface Index {
    id: string;
    isRoot: boolean;
    data: any;
    canMove: boolean;
    css: string[];
    position: XY;
    children: any[];
    parentId: number;
    connectionColor: string;
}
export interface IContainerSize {
    width: number;
    height: number;
}
export interface IItem {
    id: number;
    title: string;
    x: number;
    y: number;
}
export interface IItemPosition {
    id: number;
    x: number;
    y: number;
}
export interface IConnection {
    from: number;
    to: number;
    color?: string;
}
export interface IRootNode {
    name: string;
    size?: IContainerSize;
    color?: string;
}
export interface IConfiguration {
    rootNode: IRootNode;
    viewportPosition: XY;
    locale: string;
    items: IItem[];
    connections: IConnection[];
    colorPickerColors: string[];
    defaultContainerSize: IContainerSize;
    cell: IContainerSize;
    nodeSize: IContainerSize;
    navigatorPaneSize: IContainerSize;
    zoom: number;
    maxZoom: number;
    minZoom: number;
    lineColor: string;
    showCardActions: boolean;
    requiredUpdate: string[];
    requiredReload: string[];
}
