export var Templates;
(function (Templates) {
    Templates["viewport"] = "<div class=\"mindmap-viewport\"><div class=\"mindmap-viewport-container\" style=\"width: {0}px; height: {1}px;\" /></div>";
    Templates["zoom"] = "<div class=\"mindmap-zoom\"><div class=\"mindmap-zoom-scale\"><div class=\"mindmap-zoom-scale-selector\"><span></span></div></div></div>";
    Templates["scrollbar"] = "<div class=\"mindmap-scrollbar {0}\"><div class=\"mindmap-scrollbar-bar\" /></div>";
    Templates["rootNode"] = "<div class=\"mindmap-node isRoot\" data-id=\"{0}\" style=\"{2}\"><div class=\"mindmap-node-inner\">{1}</div><span class=\"actions\"><span class=\"action\"><span title=\"{3}\" class=\"mindmap-icon mindmap-add\"></span></span><span class=\"action\"><span title=\"{4}\" class=\"mindmap-icon mindmap-change-color\"></span></span></span></div>";
    Templates["node"] = "<div class=\"mindmap-node\" data-id=\"{0}\" style=\"{2}\"><div class=\"mindmap-node-inner\">{1}</div><span class=\"actions\"><span class=\"action\"><span title=\"{3}\" class=\"mindmap-icon mindmap-add\"></span></span><span class=\"action\"><span title=\"{4}\" class=\"mindmap-icon mindmap-remove\"></span></span></span></div>";
    Templates["navigator"] = "<div class=\"mindmap-navigator\" style=\"{0}\"></div>";
    Templates["navigatorSelector"] = "<div class=\"mindmap-navigator-selector\" style=\"{0}\"></div>";
    Templates["connectionMenu"] = "<div class=\"mindmap-menu mindmap-connection\"><span></span><span><span><span class=\"mindmap-icon mindmap-remove\"></span></span> {0}</span></div>";
    Templates["colorPicker"] = "<div class=\"mindmap-colorpicker\" style=\"top: {1}px; left: {0}px;\"><div class=\"mindmap-colorpicker-colors\"></div></div>";
})(Templates || (Templates = {}));
//# sourceMappingURL=index.js.map