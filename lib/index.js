import { Context } from './context';
import { MovementBlackBox } from './helpers/movement';
import { Zoom, Navigator, Viewport, Nodes, Connections, ConnectionMenu } from './components';
import { initializeContext, updateConfiguration } from './helpers/data';
import { Localization } from './localization';
import './styles/index.css';
import { Configuration } from './configuration';
import './assets/icons/16-plus.svg';
import './assets/icons/16-change-color.svg';
/**
 * Mindmap Control v1
 */
export default class MindMap {
    /**
     * Global Context of control
     */
    constructor(container, initialConfig) {
        /**
         * Global Context of control
         */
        this.ctx = Context.getInstance();
        this.initialize = () => {
            // Create control components
            new Zoom();
            new Viewport();
            new Navigator();
            new Connections();
            new Nodes();
            new ConnectionMenu();
            // Calculate initial zoom
            this.ctx.zoom.calculate();
            // Attach resize event
            this.ctx.attachWindowResize();
            // Refresh control adter initialization
            this.ctx.refresh();
            // Hide loader
            this.ctx.container.children[0].remove();
        };
        /**
         * Set data or configuration
         * @param config Set of props to set
         */
        this.set = (config) => updateConfiguration(config);
        /**
         * Cause refresh of control
         */
        this.refresh = () => this.ctx.refresh();
        /**
         * Cause full reload of control
         */
        this.reload = () => this.ctx.reload();
        /**
         * Dispose control and remove all components
         */
        this.dispose = () => this.ctx.dispose();
        this.destroyColorPicker = () => {
            if (this.ctx.colorPicker)
                this.ctx.colorPicker.destroy();
            //if (this.ctx.menu.current) {
            //    this.ctx.menu.current.element.classList.remove('selected')
            //    this.ctx.menu.current = null
            //}
        };
        // Prepare root container
        container.classList.add('mindmap');
        // Initialize context in subclasses
        initializeContext();
        // Create loader
        const loader = document.createElement('loader');
        loader.className = 'mindmap-loader';
        container.appendChild(loader);
        // Store important things in context
        this.ctx.container = container;
        if (initialConfig)
            updateConfiguration(initialConfig, true);
        // Init movement system
        this.ctx.movementBlackBox = new MovementBlackBox();
        this.ctx.localization = new Localization();
        this.ctx.localization.set(Configuration.locale).then(() => {
            this.initialize();
        });
    }
}
export * from './constants';
export * from './constants/interfaces';
//# sourceMappingURL=index.js.map