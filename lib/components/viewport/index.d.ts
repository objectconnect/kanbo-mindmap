import { IScrollbars, IViewportSize } from "../../constants/interfaces";
/**
 * Mindmap viewport
 */
export declare class Viewport {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Columns HTML container
     */
    container: HTMLElement;
    /**
     * Current element
     */
    element: HTMLElement;
    /**
     * Zoom selector
     */
    selector: HTMLElement;
    /**
     * Scrollbars
    */
    scrollbars: IScrollbars;
    /**
     * Current viewport size and position
     */
    size: IViewportSize;
    /**
     * Movement blackbox connection
     */
    movementInstance: any;
    hasMovement: boolean;
    /**
     * Creates an instance of zoom.
     */
    constructor();
    create: () => void;
    /**
     * Load current view size and position
     */
    getSize: () => void;
    resetPosition: () => void;
    change: (change: any, updateNavigator: any, updateScroll: any, newFactor: any, reiseGlobalEvent?: boolean) => void;
    onMovementStarted: (args: any) => void;
    onMovement: (args: any) => void;
    onMovementFinished: (args: any) => void;
    onDragOver: (args: any) => void;
    onDrop: (args: DragEvent) => void;
}
