import { Context } from '../../context';
/**
 * Mindmap viewport
 */
export class Connections {
    /**
     * Creates an instance of zoom.
     */
    constructor() {
        /**
         * Global Context of control
         */
        this.ctx = Context.getInstance();
        this.create = () => {
            // Create connections container
            this.container = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
            this.container.classList.value = 'mindmap-connections';
            this.ctx.viewport.container.prepend(this.container);
        };
        /**
         * Load current view size and position
         */
        this.render = () => { };
        this.update = (id, point, change) => {
            /*if (isNull(id) && !id) return
    
            const fromId = this.ctx.mapping.from[id]
            const toId = this.ctx.mapping.to[id]
    
            if(!isNull(fromId))
                fromId.map(fId => {
                    this.ctx.renderedElements.connections[fId].start = point
                    this.ctx.renderedElements.connections[fId].draw()
                })
    
            if(!isNull(toId))
                toId.map(tId => {
                    this.ctx.renderedElements.connections[tId].end = point
                    this.ctx.renderedElements.connections[tId].draw()
                })*/
        };
        this.ctx.connections = this;
        this.create();
        this.render();
    }
}
// TODO: nodes without connection -> root node connection
// TODO: Rerender whole connection on every node movement
//# sourceMappingURL=index.js.map