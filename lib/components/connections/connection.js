import { isNull } from "../../helpers/main";
import { Configuration } from "../../configuration";
import { Context } from '../../context';
/**
 * Mindmap viewport
 */
export class Connection {
    /**
     * Creates an instance of zoom.
     */
    constructor(data, start, end) {
        /**
         * Global Context of control
         */
        this.ctx = Context.getInstance();
        this.create = () => {
            if (!this.ctx.connections)
                return;
            // Create path element
            this.element = document.createElementNS('http://www.w3.org/2000/svg', 'path');
            this.ctx.connections.container.appendChild(this.element);
            // Attach click event
            this.element.onclick = this.onClick;
            if (this.data.color) {
                this.element.style.stroke = this.data.color;
                this.color = this.data.color;
            }
            else {
                this.element.style.stroke = Configuration.rootNode.color;
                this.color = Configuration.rootNode.color;
            }
            // Draw initial connection
            this.draw();
        };
        /**
         * Load current view size and position
         */
        this.draw = () => {
            if (!this.start || !this.end)
                return;
            if (this.data.color !== this.color) {
                if (this.data.color) {
                    this.element.style.stroke = this.data.color;
                    this.color = this.data.color;
                }
                else {
                    this.element.style.stroke = Configuration.rootNode.color;
                    this.color = Configuration.rootNode.color;
                }
            }
            const { rootNode, nodeSize } = Configuration;
            const { width, height } = nodeSize;
            let withRoot = false;
            if (this.ctx.renderedElements.root.data.position.x === this.end.x &&
                this.ctx.renderedElements.root.data.position.y === this.end.y)
                withRoot = true;
            // Define connection points
            let p1x = this.start.x + 2, p1y = (this.start.y + height) + 1, p2x = withRoot ? this.end.x + rootNode.size.width : (this.end.x + width), p2y = withRoot ? (this.end.y + rootNode.size.height) - 2 : (this.end.y + height) + 1, pLineStart = `${this.start.x + width - 5} ${p1y} h ${-width}`, pLineEnd = withRoot ? '' : ''; //`h ${-width}`
            // Switch line side
            if (this.start.x < this.end.x) {
                p1x = (this.start.x + width) - 3;
                p1y = (this.start.y + height) + 1;
                p2x = this.end.x;
                p2y = withRoot ? (this.end.y + rootNode.size.height) - 2 : (this.end.y + height) + 1;
                pLineStart = `${this.start.x + 5} ${p1y} h ${width}`,
                    pLineEnd = withRoot ? '' : ''; //`h ${width}`
            }
            // TODO: calculate with start line
            // Calculate spaces
            const differenceX = p2x - p1x; // Space between points x
            const differenceY = p2y - p1y; // Space between points y
            const Xpart = differenceX / 4; // 1/4 of whole available space ( start horizontal line / start curve / end curve / end horizontal line )
            const Ypart = differenceY / 2; // 1/2 of whole available space
            // Create SVG path data
            const line = `M ${pLineStart} h ${Xpart} c ${Xpart / 2} -2 ${Xpart} ${Ypart * 2}, ${Xpart * 2} ${Ypart * 2} h ${Xpart} ${pLineEnd}`;
            // Set path data
            this.element.setAttribute("d", line);
            if (!isNull(this.data.color) && this.color !== this.data.color) {
                this.element.style.stroke = this.data.color;
                this.color = this.data.color;
            }
            // Update navigator
            this.ctx.navigator.updateConnection({
                startPoint: this.start,
                endPoint: this.end
            });
            this.ctx.renderedElements.connections[this.data.id] = this.data.start;
        };
        this.onClick = (args) => {
            args.preventDefault();
            args.stopPropagation();
            // Switch css classes
            if (this.element.classList.contains('selected'))
                this.element.classList.remove('selected');
            else
                this.element.classList.add('selected');
            // Toggle connection menu
            const { menu } = this.ctx;
            if (menu.current && menu.current.data.id !== this.data.id) {
                menu.current.element.classList.remove('selected');
                if (menu.isVisible)
                    menu.toggle();
            }
            // Set current connection
            this.ctx.menu.current = this;
            let hideDelete = false;
            if (isNull(this.ctx.renderedElements.nodes[this.data.start].node.data.parentId))
                hideDelete = true;
            // Toggle connection menu ( should be correct by flow )
            this.ctx.menu.toggle(args.clientX, args.clientY, hideDelete);
        };
        this.data = data;
        this.start = start;
        this.end = end;
        // Genrate DOM structure
        this.create();
    }
}
//# sourceMappingURL=connection.js.map