import { IViewportSize, XY } from "../../constants/interfaces";
/**
 * Mindmap viewport
 */
export declare class Connections {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Columns HTML container
     */
    container: any;
    /**
     * Current viewport size and position
     */
    size: IViewportSize;
    /**
     * Creates an instance of zoom.
     */
    constructor();
    create: () => void;
    /**
     * Load current view size and position
     */
    render: () => void;
    update: (id: number, point?: XY | undefined, change?: XY | undefined) => void;
}
