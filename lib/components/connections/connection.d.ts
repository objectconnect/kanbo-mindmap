import { XY } from "../../constants/interfaces";
/**
 * Mindmap viewport
 */
export declare class Connection {
    /**
     * Global Context of control
     */
    private ctx;
    data: any;
    color: string;
    /**
     * Columns HTML container
     */
    element: any;
    start: XY;
    end: XY;
    /**
     * Creates an instance of zoom.
     */
    constructor(data: any, start: any, end: any);
    create: () => void;
    /**
     * Load current view size and position
     */
    draw: () => void;
    onClick: (args: any) => void;
}
