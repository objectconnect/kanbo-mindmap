import { Configuration } from "../../configuration";
import { generateFromTemplate } from '../../helpers/main';
import { Context } from '../../context';
import { Templates } from '../../templates';
import { reiseEvent, EVENT } from "../../events";
/**
 * Color picker control
 */
export class ColorPicker {
    /**
     * Creates an instance of color picker.
     */
    constructor(position, target, id, callback, container) {
        /**
         * Global Context of control
         */
        this.ctx = Context.getInstance();
        this.position = {};
        // Create color picker container
        this.create = (container) => {
            this.container = generateFromTemplate(Templates.colorPicker.format(this.position.x, this.position.y));
            this.list = this.container.children[0];
            if (container)
                container.appendChild(this.container);
            else {
                this.ctx.container.appendChild(this.container);
                this.container.style.position = 'absolute';
            }
        };
        /**
         * Load current view size and position
         */
        this.render = () => {
            Configuration.colorPickerColors.map(c => {
                const color = document.createElement('span');
                color.setAttribute('data-color', c);
                color.style.backgroundColor = c;
                color.onclick = this.onSelection;
                this.list.appendChild(color);
            });
            reiseEvent(EVENT.onColorPickerRender, { id: this.id, target: this.target, container: this.container });
        };
        this.onSelection = (args) => {
            reiseEvent(EVENT.onColorChanged, { id: this.id, target: this.target, color: args.target.attributes['data-color'].value });
            this.container.remove();
            this.callback && this.callback();
        };
        this.destroy = () => {
            reiseEvent(EVENT.onColorPickerDestroy, { id: this.id, target: this.target });
            this.container.remove();
            this.callback && this.callback();
        };
        this.id = id;
        this.position = position;
        this.target = target;
        this.callback = callback;
        this.create(container);
        this.render();
    }
}
//# sourceMappingURL=index.js.map