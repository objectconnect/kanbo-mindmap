import { XY } from "../../constants/interfaces";
import { COLOR_PICKER_TARGET } from "../../constants";
/**
 * Color picker control
 */
export declare class ColorPicker {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Color picker HTML container
     */
    container: any;
    list: any;
    position: XY;
    target: COLOR_PICKER_TARGET;
    id: string;
    callback: any;
    /**
     * Creates an instance of color picker.
     */
    constructor(position: XY, target: COLOR_PICKER_TARGET, id: string, callback?: any, container?: any);
    create: (container?: any) => void;
    /**
     * Load current view size and position
     */
    render: () => void;
    onSelection: (args: any) => void;
    destroy: () => void;
}
