import { XY } from "../../constants/interfaces";
/**
 * Mindmap viewport
 */
export declare class Node {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Current element
     */
    element: HTMLElement;
    actions: HTMLElement;
    newAction: HTMLElement;
    deleteAction: HTMLElement;
    changeColorAction: HTMLElement;
    isRemoved: boolean;
    /**
     * Current element position
     */
    position: XY;
    /**
     * Current element data
    */
    data: any;
    /**
     * Node connection ( always connected / if none then root )
     */
    connection: any;
    newRelationTarget: any;
    /**
     * Movement blackbox connection
     */
    movementInstance: any;
    /**
     * Creates an instance of node ( item ).
     */
    constructor(data: any);
    create: () => void;
    css: () => string;
    /**
     * Update current node
     */
    update: (args?: any, node?: any) => void;
    remove: () => void;
    new: (args: any) => void;
    delete: (args: any) => void;
    updateChildNodes: (args?: any) => void;
    changeColor: (args: any) => void;
    createConnection: () => void;
    setNewRelationTarget: (target: any, id: any) => boolean;
    onMovement: (args: any) => void;
    onMovementFinished: (args: any) => void;
}
