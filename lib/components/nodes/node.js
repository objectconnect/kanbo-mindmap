import { Configuration } from "../../configuration";
import { generateFromTemplate, isCollide, isNull } from '../../helpers/main';
import { Context } from '../../context';
import { Templates } from '../../templates';
import { MovementHelper } from "../../helpers/movement";
import { Connection } from "../connections/connection";
import { reiseEvent, EVENT } from "../../events";
import { ColorPicker } from "../colorpicker";
import { COLOR_PICKER_TARGET } from "../../constants";
import { strings } from '../../localization';
/**
 * Mindmap viewport
 */
export class Node {
    /**
     * Creates an instance of node ( item ).
     */
    constructor(data) {
        /**
         * Global Context of control
         */
        this.ctx = Context.getInstance();
        this.create = () => {
            // Genrate DOM structure
            if (this.data.isRoot)
                this.element = generateFromTemplate(Templates.rootNode.format(this.data.id, this.data.data.title, this.css(), strings.add_new_action, strings.change_color_action));
            else
                this.element = generateFromTemplate(Templates.node.format(this.data.id, this.data.data.title, this.css(), strings.add_new_action, strings.delete_action));
            this.ctx.viewport.container.appendChild(this.element);
            // Create node connection
            this.createConnection();
            reiseEvent(EVENT.onItemRender, {
                id: this.data.id,
                container: this.element.children[0]
            });
            this.actions = this.element.children[1];
            this.newAction = this.actions.children[0];
            this.newAction.onclick = this.new;
            if (this.data.isRoot) {
                this.changeColorAction = this.actions.children[1];
                this.changeColorAction.onclick = this.changeColor;
            }
            else {
                this.deleteAction = this.actions.children[1];
                this.deleteAction.onclick = this.delete;
            }
            // If node cannot be moved skip movement class initialization
            if (!this.data.canMove)
                return;
            this.movementInstance = new MovementHelper(this.ctx.viewport.container, this.element, true);
            this.movementInstance.set({
                actionCallback: this.onMovement,
                finishCallback: this.onMovementFinished,
                css: 'onUserInteraction',
                directAction: true,
                absoluteChange: true,
                useZoomByCalculation: true
            });
            this.isRemoved = false;
        };
        this.css = () => {
            const nodeWidth = this.data.isRoot ? Configuration.rootNode.size.width : Configuration.nodeSize.width;
            const nodeHeight = this.data.isRoot ? Configuration.rootNode.size.height : Configuration.nodeSize.height;
            const color = !isNull(Configuration.rootNode.color) ? Configuration.rootNode.color : '#dfdfdf';
            let result = `top: ${this.data.position.y}px; left: ${this.data.position.x}px; width: ${nodeWidth}px; height: ${nodeHeight}px;`;
            if (this.data.isRoot) {
                result += `background-color: ${color} !important;`;
            }
            return result;
        };
        /**
         * Update current node
         */
        this.update = (args, node) => {
            if (this.isRemoved)
                this.ctx.viewport.container.appendChild(this.element);
            let parentNode;
            if (node) {
                // Update current data
                this.data = node;
                // Find parent node
                parentNode = (!isNull(this.data.parentId) && this.data.parentId !== -1) ?
                    this.ctx.data.nodes[this.data.parentId] ? this.ctx.data.nodes[this.data.parentId]
                        : this.ctx.renderedElements.root.data
                    : this.ctx.renderedElements.root.data;
                this.connection.data = {
                    id: '_{0}_{1}_'.format(this.data.id, parentNode.id),
                    start: this.data.id,
                    end: parentNode.id,
                    color: this.data.connectionColor
                };
            }
            if (args) {
                // Apply position change - node data ( from parent )
                this.data.position.x += args.change.x;
                this.data.position.y += args.change.y;
                this.ctx.positions[this.data.id] = { id: this.data.id, ...this.data.position };
            }
            const { position } = this.data;
            if (this.ctx.hasUpdate)
                this.ctx.currentUpdate[this.data.id] = { id: this.data.id, x: position.x + (Configuration.nodeSize.width / 2), y: position.y };
            // Apply position change - DOM element
            this.element.style.top = `${position.y}px`;
            this.element.style.left = `${position.x}px`;
            // Update connection to parent node
            this.connection.start = position;
            if (args)
                this.connection.end = args.position;
            else if (node)
                this.connection.end = parentNode.position;
            this.connection.draw();
            if (args)
                this.updateChildNodes({
                    change: args.change,
                    position,
                    mouse: args.mouse
                });
        };
        this.remove = () => {
            this.element.remove();
            this.isRemoved = true;
            this.updateChildNodes();
        };
        this.new = (args) => {
            args.preventDefault();
            args.stopPropagation();
            const newPosition = this.ctx.nodes.findNewPosition(this.data.position);
            reiseEvent(EVENT.onNewItem, { container: this.newAction, parentId: this.data.id, ...newPosition });
        };
        this.delete = (args) => {
            if (args) {
                args.stopPropagation();
                args.preventDefault();
            }
            reiseEvent(EVENT.onItemDeleted, { id: this.data.id });
        };
        this.updateChildNodes = (args) => {
            if (this.ctx.renderedElements.nodes[this.data.id]) {
                this.ctx.renderedElements.nodes[this.data.id].node.data.children.forEach(child => {
                    this.ctx.renderedElements.nodes[child].node.update(args);
                });
            }
        };
        this.changeColor = (args) => {
            if (this.ctx.colorPicker) {
                this.ctx.colorPicker.destroy();
                this.ctx.colorPicker = null;
            }
            const callback = () => {
                this.ctx.colorPicker = null;
            };
            this.ctx.colorPicker = new ColorPicker({ x: args.clientX - this.ctx.viewport.size.parentLeft, y: args.clientY - this.ctx.viewport.size.parentTop }, COLOR_PICKER_TARGET.ROOT_NODE, this.data.id, callback);
        };
        this.createConnection = () => {
            // Root node has no connection
            if (this.data.isRoot)
                return;
            // Find parent node
            const parentNode = (!isNull(this.data.parentId) && this.data.parentId !== -1) ?
                this.ctx.data.nodes[this.data.parentId] ? this.ctx.data.nodes[this.data.parentId].position
                    : this.ctx.renderedElements.root.data.position
                : this.ctx.renderedElements.root.data.position;
            // Define start and end point
            const startPoint = this.data.position;
            const endPoint = parentNode;
            // Make connection ( as node need to be always connected )
            this.connection = new Connection({
                id: '_{0}_{1}_'.format(this.data.id, !isNull(this.data.parentId) && this.ctx.data.nodes[this.data.parentId] ? this.data.parentId : -1),
                start: this.data.id,
                end: this.ctx.data.nodes[this.data.parentId] ? this.data.parentId : -1,
                color: this.data.connectionColor
            }, startPoint, endPoint);
        };
        this.setNewRelationTarget = (target, id) => {
            // If still this same target do nothing
            if (this.newRelationTarget == id)
                return true;
            // If there is already any target
            if (this.newRelationTarget && this.ctx.renderedElements.nodes[this.newRelationTarget]) {
                // Clear old target
                this.ctx.renderedElements.nodes[this.newRelationTarget].node.element.classList.remove('onDrop');
                // Set new target in context
                this.newRelationTarget = id;
                // Add right styling to element
                this.element.classList.add('onDrop');
                target.parentElement.classList.add('onDrop');
            }
            else {
                this.newRelationTarget = target.parentElement.attributes['data-id'].value;
                target.parentElement.classList.add('onDrop');
                this.element.classList.add('onDrop');
            }
            return true;
        };
        this.onMovement = (args) => {
            // Block element for click event
            this.element.classList.add('mindmap-dragging');
            // Save current node position
            this.data.position = args.position;
            this.ctx.hasUpdate = true;
            this.ctx.currentUpdate[this.data.id] = { id: this.data.id, x: this.data.position.x + (Configuration.nodeSize.width / 2), y: this.data.position.y };
            // Update navigator nodes and connections
            this.ctx.navigator.update(this.data.id, args.position);
            // Check if user is creating new relationship
            var newRelationTargetExists = false;
            const selectedNode = document.elementsFromPoint(args.mouse.x, args.mouse.y).find(n => n.tagName.toLowerCase() === 'div' && n.className.indexOf('mindmap-node-inner') !== -1 &&
                (n.parentElement ? n.parentElement.attributes['data-id'].value !== this.data.id.toString() : true));
            if (selectedNode && selectedNode.parentElement)
                newRelationTargetExists = this.setNewRelationTarget(selectedNode, selectedNode.parentElement.attributes['data-id'].value);
            // Check if user cancelled new relation
            if (this.newRelationTarget && !newRelationTargetExists) {
                if (this.newRelationTarget == "-1")
                    this.ctx.renderedElements.root.element.classList.remove('onDrop');
                else
                    this.ctx.renderedElements.nodes[this.newRelationTarget].node.element.classList.remove('onDrop');
                this.newRelationTarget = null;
                this.element.classList.remove('onDrop');
            }
            // Update all child nodes ( connections )
            this.updateChildNodes(args);
            // Redraw current node parent connection
            this.connection.start = args.position;
            this.connection.draw();
        };
        this.onMovementFinished = (args) => {
            // Check if node is not coliding with root node
            const collisionWithRoot = isCollide({
                ...this.ctx.renderedElements.root.data.position,
                ...Configuration.rootNode.size
            }, { ...this.data.position, width: Configuration.nodeSize.width, height: 50 });
            if (collisionWithRoot) {
                const oldPosition = this.data.position;
                const newPosition = this.ctx.nodes.findNewPosition(oldPosition);
                this.data.position = newPosition;
                this.ctx.currentUpdate[this.data.id] = { id: this.data.id, x: this.data.position.x + (Configuration.nodeSize.width / 2), y: this.data.position.y };
                this.updateChildNodes({
                    change: { x: newPosition.x - oldPosition.x, y: newPosition.y - oldPosition.y },
                    position: this.data.position
                });
            }
            if (this.ctx.hasUpdate) {
                reiseEvent(EVENT.onViewChanged, Object.values(this.ctx.currentUpdate));
                this.ctx.hasUpdate = false;
                this.ctx.currentUpdate = {};
            }
            this.ctx.positions[this.data.id] = { id: this.data.id, ...this.data.position };
            if (this.element.classList.contains('mindmap-dragging'))
                this.element.classList.remove('mindmap-dragging');
            // Check if user want create new relation
            if (this.newRelationTarget) {
                // Remove child from old parent if not root
                if (!isNull(this.data.parentId) && this.data.parentId !== -1) {
                    const currentParent = this.ctx.renderedElements.nodes[this.data.parentId];
                    if (currentParent && !currentParent.isRemoved)
                        currentParent.node.data.children = [...currentParent.node.data.children.filter(child => child !== this.data.id)];
                }
                if (this.data.children.indexOf(parseInt(this.newRelationTarget)) !== -1) {
                    this.data.children = this.data.children.filter(ch => ch !== parseInt(this.newRelationTarget));
                    this.ctx.renderedElements.nodes[this.newRelationTarget].node.data.parentId = -1;
                    this.ctx.renderedElements.nodes[this.newRelationTarget].node.connection.end = this.ctx.renderedElements.root.data.position;
                    this.ctx.renderedElements.nodes[this.newRelationTarget].node.connection.draw();
                }
                // Reset parent info
                this.data.parentId = null;
                // Get new parent node
                let parentNode;
                if (this.newRelationTarget != '-1') {
                    // Get node
                    parentNode = this.ctx.renderedElements.nodes[this.newRelationTarget];
                    // Add new child to parent
                    parentNode.node.data.children.push(this.data.id);
                    // Set node new parent
                    this.data.parentId = this.newRelationTarget;
                    // Get node object
                    parentNode = parentNode.node;
                }
                else
                    parentNode = this.ctx.renderedElements.root;
                // Restart new relation logic
                this.newRelationTarget = null;
                // Remove interaction styling
                parentNode.element.classList.remove('onDrop');
                this.element.classList.remove('onDrop');
                const newPosition = this.ctx.nodes.findNewPosition(parentNode.data.position);
                this.data.position = newPosition;
                this.ctx.positions[this.data.id] = { id: this.data.id, ...newPosition };
                // Get node at left side of parent
                this.element.style.left = `${newPosition.x}px`;
                this.element.style.top = `${newPosition.y}px`;
                reiseEvent(EVENT.onViewChanged, [{ id: this.data.id, x: newPosition.x, y: newPosition.y }]);
                // Redraw current node parent connection
                this.connection.end = parentNode.data.position;
                this.connection.draw();
                this.updateChildNodes({
                    change: { x: 300, y: 0 },
                    position: this.data.position
                });
                reiseEvent(EVENT.onNewConnection, {
                    start: parseInt(this.data.id),
                    end: parseInt(this.data.parentId)
                });
            }
        };
        this.data = data;
        this.create();
    }
}
//# sourceMappingURL=node.js.map