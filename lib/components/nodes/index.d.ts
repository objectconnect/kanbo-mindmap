/**
 * Mindmap viewport
 */
export declare class Nodes {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Columns HTML container
     */
    container: HTMLElement;
    /**
     * Current element
     */
    element: HTMLElement;
    rootNode: any;
    /**
     * Creates an instance of zoom.
     */
    constructor();
    /**
     * Load current view size and position
     */
    render: () => void;
    clear: () => void;
    renderRootNode: () => void;
    findNewPosition: (position: any) => any;
}
