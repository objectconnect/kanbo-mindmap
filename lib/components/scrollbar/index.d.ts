import { SCROLLBAR_DIRECTION } from "../../constants";
import { IScollbar } from "../../constants/interfaces";
/**
 * Mindmap viewport
 */
export declare class ScrollBar {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Parent container
     */
    container: any;
    /**
      * Current scrollbar container
      */
    element: any;
    /**
     * Scrollbar bar
     */
    bar: HTMLElement;
    /**
     * Scrollbar direction
     */
    direction: SCROLLBAR_DIRECTION;
    /**
     * Scroll callback
     */
    callback: any;
    /**
     * Bar size and available size
     */
    size: number;
    max: number;
    /**
     * Scrollbar position
     */
    position: IScollbar;
    /**
     * Movement blackbox connection
     */
    movementInstance: any;
    /**
     * Creates an instance of zoom.
     */
    constructor(container: any, direction: SCROLLBAR_DIRECTION, barSize: number, availableSize: number, callback: any);
    create: () => void;
    /**
     * Load current view size and position
     */
    configure: () => void;
    set: (newPosition: number) => void;
    update: () => void;
    onMovement: (args: any) => void;
}
