import { generateFromTemplate } from '../../helpers/main';
import { Context } from '../../context';
import { Templates } from '../../templates';
import { SCROLLBAR_DIRECTION } from "../../constants";
import { MovementHelper } from "../../helpers/movement";
/**
 * Mindmap viewport
 */
export class ScrollBar {
    /**
     * Creates an instance of zoom.
     */
    constructor(container, direction, barSize, availableSize, callback) {
        /**
         * Global Context of control
         */
        this.ctx = Context.getInstance();
        this.create = () => {
            // Genrate DOM structure
            this.element = generateFromTemplate(Templates.scrollbar.format(SCROLLBAR_DIRECTION[this.direction]));
            this.ctx.container.appendChild(this.element);
            // Get bar element
            this.bar = this.element.children[0];
            this.movementInstance = new MovementHelper(this.element, this.bar);
            this.movementInstance.set({
                actionCallback: this.onMovement,
                customCellSize: { width: 1, height: 1 },
                useCustomCellSize: true,
                absoluteChange: true,
                directAction: true,
                direction: this.direction
            }); // TODO: vertical ?
        };
        /**
         * Load current view size and position
         */
        this.configure = () => {
            if (this.direction === SCROLLBAR_DIRECTION.HORIZONTAL) {
                this.bar.style.width = `${this.size}px`;
                this.bar.style.left = `${this.max}px`;
            }
            else if (this.direction === SCROLLBAR_DIRECTION.VERTICAL) {
                this.bar.style.height = `${this.size}px`;
                this.bar.style.top = `${this.max}px`;
            }
        };
        this.set = (newPosition) => {
            if (this.direction === SCROLLBAR_DIRECTION.HORIZONTAL)
                this.bar.style.left = `${Math.abs(newPosition)}px`;
            else if (this.direction === SCROLLBAR_DIRECTION.VERTICAL)
                this.bar.style.top = `${Math.abs(newPosition)}px`;
        };
        this.update = () => {
            if (this.direction === SCROLLBAR_DIRECTION.HORIZONTAL) {
                this.bar.style.width = `${this.size / this.ctx.zoom.zoom.current}px`;
                this.bar.style.left = `${Math.abs((this.ctx.viewport.container.offsetLeft + this.ctx.zoom.zoom.diff.x) / this.ctx.viewport.scrollbars.horizontalFactor)}px`;
            }
            else if (this.direction === SCROLLBAR_DIRECTION.VERTICAL) {
                this.bar.style.height = `${this.size / this.ctx.zoom.zoom.current}px`;
                this.bar.style.top = `${Math.abs((this.ctx.viewport.container.offsetTop + this.ctx.zoom.zoom.diff.y) / this.ctx.viewport.scrollbars.verticalFactor)}px`;
            }
        };
        this.onMovement = (args) => {
            if (this.callback) {
                this.callback(args.change, true, false, null);
            }
        };
        this.container = container;
        this.direction = direction;
        this.callback = callback;
        this.size = barSize;
        this.max = availableSize;
        this.create();
        this.configure();
    }
}
//# sourceMappingURL=index.js.map