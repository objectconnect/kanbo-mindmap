/**
 * Mindmap Navigator
 */
export declare class Navigator {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Canvas and its context
     */
    canvas: HTMLCanvasElement;
    canvas2Dctx: any;
    /**
     * Current element
     */
    element: HTMLElement;
    /**
     * Zoom selector
     */
    selector: HTMLElement;
    /**
     * Currently visible data
    */
    data: any;
    horizontalScrollFactor: any;
    verticalScrollFactor: any;
    drawingHorizontalFactor: any;
    drawingVerticalFactor: any;
    /**
     * Movement blackbox connection
     */
    movementInstance: any;
    /**
     * Creates an instance of zoom.
     */
    constructor();
    update: () => void;
    drawMap: () => void;
    updateSelector: () => void;
    addConnection: (connection: any) => void;
    updateConnection: (connection: any) => void;
    drawConnection: (connection: any) => void;
    /**
     * Load current view size and position
     */
    setScaleFactor: () => void;
    setNewPosition: () => void;
    calculateSelectorSize: () => {
        width: number;
        height: number;
    };
    onMovement: (args: any) => any;
    onMovementFinished: (args: any) => any;
}
