import { Configuration } from "../../configuration";
import { generateFromTemplate } from '../../helpers/main';
import { Context } from '../../context';
import { Templates } from '../../templates';
import { MovementHelper } from "../../helpers/movement";
import { drawCircle, drawLine } from "../../helpers/canvas";
/**
 * Mindmap Navigator
 */
export class Navigator {
    /**
     * Creates an instance of zoom.
     */
    constructor() {
        /**
         * Global Context of control
         */
        this.ctx = Context.getInstance();
        /**
         * Currently visible data
        */
        this.data = {
            nodes: {},
            connections: {}
        };
        this.update = () => {
            // Clear canvas ( everything need to be redraw )
            this.canvas2Dctx.clearRect(0, 0, Configuration.navigatorPaneSize.width, Configuration.navigatorPaneSize.height);
            // Draw canvas with updated data
            this.drawMap();
        };
        this.drawMap = () => {
            Object.keys(this.ctx.renderedElements.nodes).map(key => {
                const node = this.ctx.renderedElements.nodes[key];
                if (!node)
                    return;
                drawCircle(this.canvas2Dctx, (node.node.data.position.x + 125) / this.drawingHorizontalFactor, (node.node.data.position.y + 50) / this.drawingVerticalFactor);
                this.drawConnection(node.node.connection);
            });
            //Object.keys(this.ctx.data.connections).forEach(val =>
            //this.drawConnection(this.data.connections[val]))
        };
        this.updateSelector = () => {
            // Calculate new Size of selector
            const selectorSize = this.calculateSelectorSize();
            // Set new size to selector DOM element
            this.selector.style.width = `${selectorSize.width}px`;
            this.selector.style.height = `${selectorSize.height}px`;
            // Recalculate scroll factor for navigator
            this.horizontalScrollFactor = (this.ctx.viewport.size.containerWidth * this.ctx.zoom.zoom.current) / Configuration.navigatorPaneSize.width;
            this.verticalScrollFactor = (this.ctx.viewport.size.containerHeight * this.ctx.zoom.zoom.current) / Configuration.navigatorPaneSize.height;
            // Set selector new position
            this.setNewPosition();
        };
        this.addConnection = (connection) => {
            // Save connection data to memory
            //this.ctx.data.connections[connection.id] = connection
            // Draw new connection
            this.drawConnection(connection);
        };
        this.updateConnection = (connection) => {
            //this.ctx.data.connections[connection.id] = connection
        };
        this.drawConnection = (connection) => {
            if (!connection)
                return;
            var scaledStartPoint = {
                x: (connection.start.x + 125) / this.drawingHorizontalFactor,
                y: (connection.start.y + 50) / this.drawingVerticalFactor,
            };
            var scaledEndPoint = {
                x: (connection.end.x + 125) / this.drawingHorizontalFactor,
                y: (connection.end.y + 50) / this.drawingVerticalFactor
            };
            drawLine(this.canvas2Dctx, scaledStartPoint, scaledEndPoint);
        };
        /**
         * Load current view size and position
         */
        this.setScaleFactor = () => {
            this.drawingHorizontalFactor = this.ctx.viewport.size.containerWidth / Configuration.navigatorPaneSize.width;
            this.drawingVerticalFactor = this.ctx.viewport.size.containerHeight / Configuration.navigatorPaneSize.height;
            this.horizontalScrollFactor = this.drawingHorizontalFactor;
            this.verticalScrollFactor = this.drawingVerticalFactor;
        };
        this.setNewPosition = () => {
            const { container } = this.ctx.viewport;
            const posLeft = (container.offsetLeft + this.ctx.zoom.zoom.diff.x) / this.horizontalScrollFactor;
            const posTop = (container.offsetTop + this.ctx.zoom.zoom.diff.y) / this.verticalScrollFactor;
            this.selector.style.left = posLeft < 0 ? `${-posLeft}px` : '0px';
            this.selector.style.top = posTop < 0 ? `${-posTop}px` : '0px';
        };
        this.calculateSelectorSize = () => {
            const { size } = this.ctx.viewport;
            const scaleFactorX = (size.containerWidth * this.ctx.zoom.zoom.current) / size.width;
            const scaleFactorY = (size.containerHeight * this.ctx.zoom.zoom.current) / size.height;
            return {
                width: Configuration.navigatorPaneSize.width / (scaleFactorX < 1 ? 1 : scaleFactorX),
                height: Configuration.navigatorPaneSize.height / (scaleFactorY < 1 ? 1 : scaleFactorY)
            };
        };
        this.onMovement = (args) => this.ctx.viewport.change(args.change, false, true, {
            horizontal: this.horizontalScrollFactor,
            vertical: this.verticalScrollFactor
        }, false);
        this.onMovementFinished = (args) => this.ctx.viewport.change(args.change, false, true, {
            horizontal: this.horizontalScrollFactor,
            vertical: this.verticalScrollFactor
        }, true);
        this.ctx.navigator = this;
        // Genrate DOM structure
        this.element = generateFromTemplate(Templates.navigator.format(`width: ${Configuration.navigatorPaneSize.width}px; height: ${Configuration.navigatorPaneSize.height}px;`));
        // Create canvas element
        this.canvas = document.createElement('canvas');
        // Set proper canvas size
        this.canvas.setAttribute("width", "250");
        this.canvas.setAttribute("height", "200");
        // Append element to control
        this.ctx.container.appendChild(this.element);
        this.setScaleFactor();
        // Calculate selector size
        const selectorSize = this.calculateSelectorSize();
        // Generate selector element
        this.selector = generateFromTemplate(Templates.navigatorSelector.format(`width: ${selectorSize.width}px; height: ${selectorSize.height}px;` +
            `top: ${Math.abs((this.element.clientHeight - selectorSize.height) / 2)}px;` +
            `left: ${Math.abs((this.element.clientWidth - selectorSize.width) / 2)}px`));
        // Append subcontrol to current element
        this.element.appendChild(this.canvas);
        this.element.appendChild(this.selector);
        // Load canvas context
        this.canvas2Dctx = this.canvas.getContext('2d');
        this.movementInstance = new MovementHelper(this.element, this.selector);
        this.movementInstance.set({
            actionCallback: this.onMovement,
            finishCallback: this.onMovementFinished,
            absoluteChange: true,
            directAction: true
        }); // TODO: vertical ?
    }
}
//# sourceMappingURL=index.js.map