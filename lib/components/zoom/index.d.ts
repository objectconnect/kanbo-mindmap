import { IZoom } from '../../constants/interfaces';
/**
 * Mindmap zoom
 */
export declare class Zoom {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Columns HTML container
     */
    container: any;
    /**
     * Current element
     */
    element: any;
    /**
     * Zoom selector
     */
    selector: HTMLElement;
    /**
     * Currently applied zoom factor
     */
    zoom: IZoom;
    /**
     * Movement blackbox connection
     */
    movementInstance: any;
    /**
     * Creates an instance of zoom.
     */
    constructor();
    /**
     * Create base structure and attach events
     */
    create: () => void;
    /**
     * Calculate zoom difference
     */
    calculate: () => void;
    define: () => void;
    calculateSliderPosition: () => number;
    update: (zoomValue: number, globalEvent?: boolean) => void;
    /**
     * On zoom movement
     */
    onMovement: (args: any) => void;
    onMovementFinished: (args: any) => void;
}
