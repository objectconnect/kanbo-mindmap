import { Configuration } from '../../configuration';
import { generateFromTemplate } from '../../helpers/main';
import { Context } from '../../context';
import { Templates } from '../../templates';
import { MovementHelper } from '../../helpers/movement';
import { MOVEMENT_DIRECTION } from '../../constants';
import { EVENT, reiseEvent } from '../../events';
/**
 * Mindmap zoom
 */
export class Zoom {
    /**
     * Creates an instance of zoom.
     */
    constructor() {
        /**
         * Global Context of control
         */
        this.ctx = Context.getInstance();
        /**
         * Currently applied zoom factor
         */
        this.zoom = { diff: {}, factor: {} };
        /**
         * Create base structure and attach events
         */
        this.create = () => {
            // Genrate DOM structure
            this.element = generateFromTemplate(Templates.zoom);
            this.ctx.container.appendChild(this.element);
            // Load subcontrols
            this.container = this.element.children[0];
            this.selector = this.container.children[0];
            this.movementInstance = new MovementHelper(this.container, this.selector);
            this.movementInstance.set({
                actionCallback: this.onMovement,
                finishCallback: this.onMovementFinished,
                directAction: true,
                absoluteChange: true,
                direction: MOVEMENT_DIRECTION.VERTICAL
            });
        };
        /**
         * Calculate zoom difference
         */
        this.calculate = () => {
            const width = this.ctx.viewport.size.containerWidth;
            const height = this.ctx.viewport.size.containerHeight;
            this.zoom.diff.x = (width - (width * this.zoom.current)) / 2;
            this.zoom.diff.y = (height - (height * this.zoom.current)) / 2;
        };
        this.define = () => {
            this.zoom.factor.size = this.container.clientHeight - this.selector.clientHeight;
            // Set max factor
            this.zoom.factor.max = this.zoom.factor.size * (this.zoom.max - 1);
            // Set scale factor
            this.zoom.factor.scale = (this.zoom.max - 1) + (1 - this.zoom.min);
            // Set max top
            this.zoom.factor.top = this.zoom.factor.max / this.zoom.factor.scale;
            // Set initial zoom value
            this.selector.children[0]['innerText'] = "{0}x".format(parseFloat(this.zoom.current.toString()).toFixed(1));
            // Set initial top position
            this.selector.style.top = `${this.calculateSliderPosition()}px`;
        };
        this.calculateSliderPosition = () => {
            const result = this.zoom.factor.size * (((this.zoom.current - this.zoom.min) / (this.zoom.max - this.zoom.min)));
            return result;
        };
        this.update = (zoomValue, globalEvent = false) => {
            const { max, min } = this.zoom;
            // Check new value
            if (zoomValue > max)
                zoomValue = this.zoom.max;
            if (zoomValue < min)
                zoomValue = this.zoom.min;
            if (globalEvent) {
                Configuration.zoom = zoomValue;
                reiseEvent(EVENT.onZoomChanged, { zoom: zoomValue });
            }
            // Update current zoom value
            this.zoom.current = zoomValue;
            this.define();
            this.calculate();
            this.ctx.refresh();
        };
        /**
         * On zoom movement
         */
        this.onMovement = (args) => {
            const { max, min } = this.zoom;
            let newPosition = 1 + ((1 - max) * (1 - (args.position.y / this.zoom.factor.top)));
            if (newPosition > max)
                newPosition = this.zoom.max;
            if (newPosition < min)
                newPosition = this.zoom.min;
            this.zoom.current = parseFloat(newPosition.toFixed(2));
            Configuration.zoom = parseFloat(newPosition.toFixed(2));
            this.selector.children[0]['innerText'] = "{0}x".format(parseFloat(this.zoom.current.toString()).toFixed(1));
            this.calculate();
            this.ctx.refresh();
        };
        this.onMovementFinished = (args) => {
            // Set new config value
            Configuration.zoom = this.zoom.current;
            // Reise global event
            reiseEvent(EVENT.onZoomChanged, { zoom: this.zoom.current });
        };
        this.ctx.zoom = this;
        // Create control
        this.create();
        // Set max
        this.zoom.max = Configuration.maxZoom;
        // Set min
        this.zoom.min = Configuration.minZoom;
        // Set initial zoom value
        this.zoom.current = Configuration.zoom;
        this.define();
    }
}
//# sourceMappingURL=index.js.map