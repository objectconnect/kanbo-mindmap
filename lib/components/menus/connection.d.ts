/**
 * Menu for connection
 */
export declare class ConnectionMenu {
    /**
     * Global Context of control
     */
    private ctx;
    /**
     * Current element
     */
    element: HTMLElement;
    /**
     * Current selected connection
     */
    current: any;
    /**
     * Visibility state
    */
    isVisible: boolean;
    deleteAction: any;
    changeColorAction: any;
    /**
     * Creates an instance of zoom.
     */
    constructor();
    create: () => void;
    recreate: () => void;
    changeConnectionColor: (args: any) => void;
    /**
     * Load current view size and position
     */
    deleteConnection: () => void;
    toggle: (x?: any, y?: any) => void;
}
