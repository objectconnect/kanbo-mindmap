export declare const Configuration: {
    rootNode: {
        name: string;
        color: string;
        size: {
            width: number;
            height: number;
        };
    };
    viewportPosition: {
        x: number;
        y: number;
    };
    colorPickerColors: string[];
    defaultContainerSize: {
        width: number;
        height: number;
    };
    cell: {
        width: number;
        height: number;
    };
    nodeSize: {
        width: number;
        height: number;
    };
    rootNodeSize: {
        width: number;
        height: number;
    };
    rootNodeColor: string;
    navigatorPaneSize: {
        width: number;
        height: number;
    };
    zoom: number;
    maxZoom: number;
    minZoom: number;
    lineColor: string;
    showCardActions: boolean;
    requiredUpdate: string[];
    locale: string;
    requiredReload: string[];
};
